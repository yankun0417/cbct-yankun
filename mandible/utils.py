from __future__ import print_function

import scipy.sparse as sp
import numpy as np
from scipy.sparse.linalg.eigen.arpack import eigsh, ArpackNoConvergence
from scipy import ndimage
from random import randint, choice

import h5py
import os
import random
import SimpleITK as sitk

def encode_onehot(labels):
    classes = set(labels)
    classes_dict = {c: np.identity(len(classes))[i, :] for i, c in enumerate(classes)}
    labels_onehot = np.array(list(map(classes_dict.get, labels)), dtype=np.int32)
    return labels_onehot


def load_data(path="data/cora/", dataset="cora"):
    """Load citation network dataset (cora only for now)"""
    print('Loading {} dataset...'.format(dataset))

    idx_features_labels = np.genfromtxt("{}{}.content".format(path, dataset), dtype=np.dtype(str))
    features = sp.csr_matrix(idx_features_labels[:, 1:-1], dtype=np.float32)
    labels = encode_onehot(idx_features_labels[:, -1])
    SYM_NORM = True
    MAX_DEGREE = 2
    # build graph
    idx = np.array(idx_features_labels[:, 0], dtype=np.int32)
    idx_map = {j: i for i, j in enumerate(idx)}
    edges_unordered = np.genfromtxt("{}{}.cites".format(path, dataset), dtype=np.int32)
    edges = np.array(list(map(idx_map.get, edges_unordered.flatten())),
                     dtype=np.int32).reshape(edges_unordered.shape)
    adj = sp.coo_matrix((np.ones(edges.shape[0]), (edges[:, 0], edges[:, 1])),
                        shape=(labels.shape[0], labels.shape[0]), dtype=np.float32)
    #print('adj is ', adj)

    # build symmetric adjacency matrix
    adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)

    print('Dataset has {} nodes, {} edges, {} features.'.format(adj.shape[0], edges.shape[0], features.shape[1]))
    #print(adj.todense().shape)
    return features.todense(), adj, labels


def normalize_adj(adj, symmetric=True):
    if symmetric:
        m = np.array([[1,2,3],[2,1,4],[3,4,1]])
       # print(m.sum(1))
       # print(np.power(m.sum(1),-0.5).flatten())
        d = sp.diags(np.power(np.array(adj.sum(1)), -0.5).flatten(), 0)
        a_norm = adj.dot(d).transpose().dot(d).tocsr()
    else:
        d = sp.diags(np.power(np.array(adj.sum(1)), -1).flatten(), 0)
        a_norm = d.dot(adj).tocsr()
    return a_norm


def preprocess_adj(adj, symmetric=True):
    adj = adj + sp.eye(adj.shape[0])
    adj = normalize_adj(adj, symmetric)
    return adj


def sample_mask(idx, l):
    mask = np.zeros(l)
    mask[idx] = 1
    return np.array(mask, dtype=np.bool)


def get_splits(y):
    idx_train = range(140)
    idx_val = range(200, 500)
    idx_test = range(500, 1500)
    y_train = np.zeros(y.shape, dtype=np.int32)
    y_val = np.zeros(y.shape, dtype=np.int32)
    y_test = np.zeros(y.shape, dtype=np.int32)
    y_train[idx_train] = y[idx_train]
    y_val[idx_val] = y[idx_val]
    y_test[idx_test] = y[idx_test]
    train_mask = sample_mask(idx_train, y.shape[0])
    return y_train, y_val, y_test, idx_train, idx_val, idx_test, train_mask


def categorical_crossentropy(preds, labels):
    return np.mean(-np.log(np.extract(labels, preds)))


def accuracy(preds, labels):
    return np.mean(np.equal(np.argmax(labels, 1), np.argmax(preds, 1)))


def evaluate_preds(preds, labels, indices):

    split_loss = list()
    split_acc = list()

    for y_split, idx_split in zip(labels, indices):
        split_loss.append(categorical_crossentropy(preds[idx_split], y_split[idx_split]))
        split_acc.append(accuracy(preds[idx_split], y_split[idx_split]))

    return split_loss, split_acc


def normalized_laplacian(adj, symmetric=True):
    adj_normalized = normalize_adj(adj, symmetric)
    laplacian = sp.eye(adj.shape[0]) - adj_normalized
    return laplacian


def rescale_laplacian(laplacian):
    try:
        print('Calculating largest eigenvalue of normalized graph Laplacian...')
        largest_eigval = eigsh(laplacian, 1, which='LM', return_eigenvectors=False)[0]
        #print(largest_eigval)
    except ArpackNoConvergence:
        print('Eigenvalue calculation did not converge! Using largest_eigval=2 instead.')
        largest_eigval = 2

    scaled_laplacian = (2. / largest_eigval) * laplacian - sp.eye(laplacian.shape[0])
    return scaled_laplacian


def chebyshev_polynomial(X, k):
    """Calculate Chebyshev polynomials up to order k. Return a list of sparse matrices."""
    print("Calculating Chebyshev polynomials up to order {}...".format(k))

    T_k = list()
    T_k.append(sp.eye(X.shape[0]).tocsr())
    T_k.append(X)

    def chebyshev_recurrence(T_k_minus_one, T_k_minus_two, X):
        X_ = sp.csr_matrix(X, copy=True)
        return 2 * X_.dot(T_k_minus_one) - T_k_minus_two

    for i in range(2, k+1):
        T_k.append(chebyshev_recurrence(T_k[-1], T_k[-2], X))

    return T_k


# def sparse_to_tuple(sparse_mx):
#     if not sp.isspmatrix_coo(sparse_mx):
#         sparse_mx = sparse_mx.tocoo()
#     coords = np.vstack((sparse_mx.row, sparse_mx.col)).transpose()
#     values = sparse_mx.data
#     shape = sparse_mx.shape
#     return coords, values, shape
def data_generator(train_or_val_list, config, shuffle=False, augment=False, augmentation=None,
                   random_rois=0, batch_size=1, detection_targets=False):
    b = 0  # batch item index
    MAX_DEGREE = 2  # maximum polynomial degree
    SYM_NORM = True
    image_index = -1
    train_or_val_list = np.copy(train_or_val_list)
    image_path = './train/1.6mm/RC_HM_image/'
    lmk_path = './train/1.6mm/landmarks/'
    heatmap_path = './train/1.6mm/heatmap/0-33/'
    segmentation_path = './train/1.6mm/segmentation/'
    Sigma = [0.25, 0.5, 1, 2, 4]
    Flip = [0,0.5]
    Rotation = [-5, 0, 5,-1,-2,-3,-4,2,3,4,1]
    Dropout = [0, 0.01, 0.02, 0.03, 0.05]
    p_z = 128
    p_y = 128
    p_x = 128
    number_of_lmk = 27 #33
    padded_image_size = np.array([p_z, p_y, p_x])
    while True:
        block = np.random.choice(27,size = 14,replace = False)
        #print(block)
        S = random.choice(Sigma)
        R = random.choice(Rotation)
        D = random.choice(Dropout)
        F = random.choice(Flip)
        image_index = (image_index + 1) % len(train_or_val_list)
        if shuffle:
            np.random.shuffle(train_or_val_list)
        # Increment index to pick next image. Shuffle if at the start of an epoch.
        image_id = train_or_val_list[image_index]
        #file_path = image_path + 'HM_Rescaled_subject_{}.nii.gz'.format(image_id)
        landmark_path = lmk_path + 'subject_{}.hdf5'.format(image_id)
        #print(image_id)
        if not os.path.exists(image_path + 'HM_Rescaled_subject_{}.hdf5'.format(image_id)):
            #print(image_path + 'HM_Rescaled_subject_{}.hdf5'.format(image_id))

            continue
        else:
            f = h5py.File(image_path + 'HM_Rescaled_subject_{}.hdf5'.format(image_id), 'r')
            image_array = f['image'].value
            f.close()
            f = h5py.File(heatmap_path + 'subject_{}.hdf5'.format(image_id), 'r')
            heatmap = f['heatmaps'].value
            f.close()
            f = h5py.File(segmentation_path + 'Seg_subject_{}.hdf5'.format(image_id), 'r')
            segmentation = f['segmentation'].value
            f.close()
            # image = sitk.ReadImage(file_path)
            # image_array = sitk.GetArrayFromImage(image)  # (z,y,x)
            # image_array = (image_array / 4096.0)
            padded_image = np.zeros(padded_image_size, dtype=np.float32)
            padded_seg = np.zeros(padded_image_size, dtype=np.int32)

            bias_shape = padded_image_size - image_array.shape
            class_id = np.ones((number_of_lmk, 1), dtype=np.int32)
            if bias_shape[0] < 0:
                p_z_s = 0
                p_z_stop = padded_image_size[0]
                i_z_s = -bias_shape[0] / 2
                i_z_stop = -bias_shape[0] / 2 + padded_image_size[0]
            else:
                p_z_s = bias_shape[0] / 2
                p_z_stop = bias_shape[0] / 2 + image_array.shape[0]
                i_z_s = 0
                i_z_stop = image_array.shape[0]
            if bias_shape[1] < 0:
                p_y_s = 0
                p_y_stop = padded_image_size[1]
                i_y_s = -bias_shape[1] / 2
                i_y_stop = -bias_shape[1] / 2 + padded_image_size[1]
            else:
                p_y_s = bias_shape[1] / 2
                p_y_stop = bias_shape[1] / 2 + image_array.shape[1]
                i_y_s = 0
                i_y_stop = image_array.shape[1]
            if bias_shape[2] < 0:
                p_x_s = 0
                p_x_stop = padded_image_size[2]
                i_x_s = -bias_shape[2] / 2
                i_x_stop = -bias_shape[2] / 2 + padded_image_size[2]
            else:
                p_x_s = bias_shape[2] / 2
                p_x_stop = bias_shape[2] / 2 + image_array.shape[2]
                i_x_s = 0
                i_x_stop = image_array.shape[2]

            padded_seg[int(p_z_s):int(p_z_stop),
            int(p_y_s):int(p_y_stop),
            int(p_x_s):int(p_x_stop)] = segmentation[int(i_z_s):int(i_z_stop), int(i_y_s):int(i_y_stop),
                                        int(i_x_s):int(i_x_stop)]
            f = h5py.File(landmark_path, 'r')
            lmk = f['lmk'].value
            lmk1 = lmk[0:27]
            for i in range(number_of_lmk):
                if int(lmk1[i][0]) < 1:
                    class_id[i][0] = 0
                else:
                    class_id[i][0] = 1
            lmk_1 = lmk1 + bias_shape / 2
            padded_image[int(p_z_s):int(p_z_stop),
            int(p_y_s):int(p_y_stop),
            int(p_x_s):int(p_x_stop)] = image_array[int(i_z_s):int(i_z_stop), int(i_y_s):int(i_y_stop),
                                        int(i_x_s):int(i_x_stop)] / 4096.0
            t2 = np.zeros((number_of_lmk, p_z, p_y, p_x), np.float32)
            t2[:, int(p_z_s):int(p_z_stop),
            int(p_y_s):int(p_y_stop),
            int(p_x_s):int(p_x_stop)] = heatmap[0:27, int(i_z_s):int(i_z_stop), int(i_y_s):int(i_y_stop),
                                        int(i_x_s):int(i_x_stop)]

            # block_array = np.zeros((p_z, p_y, p_x), np.float32)
            # block = np.arange(0,27)
            # for j in range(len(block)):
            #
            #
            #     # lmk_1 = lmk1[0:number_of_lmk]
            #
            #     idx = block[j]
            #     t2[idx] = block_array
            #     class_id[idx][0] = 0
            #     block_z = int(lmk_1[idx][0])
            #     block_y = int(lmk_1[idx][1])
            #     block_x = int(lmk_1[idx][2])
            #
            #     block_z_l = np.random.choice(np.arange(6,8))
            #     block_z_h = np.random.choice(np.arange(6,8))
            #
            #     block_y_l = np.random.choice(np.arange(6,8))
            #     block_y_h = np.random.choice(np.arange(6,8))
            #
            #     block_x_l = np.random.choice(np.arange(6,8))
            #     block_x_h = np.random.choice(np.arange(6,8))
            #
            #     padded_image[block_z - block_z_l: block_z + block_z_h, block_y - block_y_l: block_y + block_y_h, block_x - block_x_l: block_x + block_x_h] = 0.0
            #     lmk_1[idx][0] = 0.0
            #     lmk_1[idx][1] = 0.0
            #     lmk_1[idx][2] = 0.0
            # for i in range(t2.shape[0]):
            #     loc = np.unravel_index(np.argmax(t2[i], axis=None), t2[i].shape)
            #     padded_seg[loc[0] - 3:loc[0] + 3, loc[1] - 3:loc[1] + 3, loc[2] - 3:loc[2] + 3] = 20
            # SN_seg = sitk.GetImageFromArray(padded_seg)
            # SN_seg.SetSpacing(np.array([1.6,1.6,1.6]))
            # sitk.WriteImage(SN_seg, 'sanity_{}.nii.gz'.format(image_id))
            # print(int(lmk_1[i][0]), int(lmk_1[i][1]), int(lmk_1[i][2]))
            # print(np.unravel_index(np.argmax(t2[i], axis=None), t2[i].shape))
            # for i in range(number_of_lmk):
            #     SN_seg = sitk.GetImageFromArray(t2[i])
            #     SN_seg.SetSpacing(np.array([1.6,1.6,1.6]))
            #     sitk.WriteImage(SN_seg, 'heatmap_{}.nii.gz'.format(i))
            # mean = np.mean(padded_image)
            # std = np.std(padded_image)
            # padded_image1 = (padded_image - mean) / (std + 1e-6)
            # padded_image1 = padded_image1/padded_image1.max()
            # SN_seg = sitk.GetImageFromArray(padded_image1)
            # SN_seg.SetSpacing(np.array([1.6,1.6,1.6]))
            # sitk.WriteImage(SN_seg, 'padded_iamge_{}.nii.gz'.format(image_id))
            # padded_image2 = (padded_image1 - padded_image1.min()) / (padded_image1.max() - padded_image1.min())
            # print(class_id)
            if b == 0:
                batch_images = np.zeros((batch_size,) + padded_image.shape, dtype=np.float32)
                batch_lmk = np.zeros((batch_size,) + lmk_1.shape, dtype=np.float32)
            batch_images[b] = padded_image.astype(np.float32)
            image_in = np.expand_dims(batch_images, axis=0)
            batch_lmk[b] = lmk_1 / padded_image_size
            batch_class = np.expand_dims(class_id, axis=0)
            # print(batch_class.shape)
            inputs = [image_in, batch_class, batch_lmk, np.expand_dims(t2, axis=0)]
            outputs = []
            # outputs = [batch_class,batch_lmk]
            # print(lmk_out/padded_image_size)
            yield inputs, outputs

        # if bias_shape[1] < 0:
        #     continue
        # else:
        #
        #     padded_image[int(bias_shape[0]):int(bias_shape[0]) + int(image_array.shape[0]),
        #     int(bias_shape[1]):int(bias_shape[1]) + int(image_array.shape[1]),
        #     int(bias_shape[2]):int(bias_shape[2]) + int(image_array.shape[2])] = image_array / 4096.0
        #
        #     padded_seg[int(bias_shape[0]):int(bias_shape[0]) + int(image_array.shape[0]),
        #     int(bias_shape[1]):int(bias_shape[1]) + int(image_array.shape[1]),
        #     int(bias_shape[2]):int(bias_shape[2]) + int(image_array.shape[2])] = segmentation
        #     f = h5py.File(landmark_path, 'r')
        #     lmk = f['lmk'].value
        #     for i in range(number_of_lmk):
        #         if int(lmk[i][0]) < 1:
        #             class_id[i][0] = 0
        #         else:
        #             class_id[i][0] = 1
        #     lmk1 = lmk + bias_shape
        #     lmk_1 = lmk1[0:number_of_lmk]
        #
        #     t2 = np.zeros((number_of_lmk, p_z, p_y, p_x), np.float32)
        #     t2[:,int(bias_shape[0]):int(bias_shape[0]) + int(image_array.shape[0]),
        #     int(bias_shape[1]):int(bias_shape[1]) + int(image_array.shape[1]),
        #     int(bias_shape[2]):int(bias_shape[2]) + int(image_array.shape[2])] = heatmap /heatmap.max()
            #print(t2.max(),t2.min())
            # for i in range(t2.shape[0]):
            #     loc = np.unravel_index(np.argmax(t2[i], axis=None), t2[i].shape)
            #     padded_seg[loc[0] - 3:loc[0] + 3, loc[1] - 3:loc[1] + 3, loc[2] - 3:loc[2] + 3] = 20
            # SN_seg = sitk.GetImageFromArray(padded_seg)
            # SN_seg.SetSpacing(np.array([1.6,1.6,1.6]))
            # sitk.WriteImage(SN_seg, 'heatmap_{}.nii.gz'.format(image_id))
                # print(int(lmk_1[i][0]), int(lmk_1[i][1]), int(lmk_1[i][2]))
                # print(np.unravel_index(np.argmax(t2[i], axis=None), t2[i].shape))
            # for i in range(number_of_lmk):
            #     SN_seg = sitk.GetImageFromArray(t2[i])
            #     SN_seg.SetSpacing(np.array([1.6,1.6,1.6]))
            #     sitk.WriteImage(SN_seg, 'heatmap_{}.nii.gz'.format(i))
            # for i in range(lmk_1.shape[0]):
            #     if int(lmk_1[i][0]) < 1:
            #         class_id[i][0] = 0
            #     else:
            #         class_id[i][0] = 1
            # landmark_array = np.zeros(padded_image.shape, dtype=np.int32)
            # lmk_out = np.zeros((number_of_lmk, 3), dtype=np.int32)
            # for i in range(26):
            #
            #     loc = lmk_1[i]
            #     if loc[0] > p_z:
            #         z = p_z-1
            #     else:
            #         z = np.int32(loc[0])
            #     if loc[0] < 0:
            #         z = 0
            #     if loc[1] > p_y:
            #         y = p_y - 1
            #     else:
            #         y = np.int32(loc[1])
            #     if loc[1] < 0:
            #         y = 0
            #     if loc[2] > p_x:
            #         x = p_x - 1
            #     else:
            #         x = np.int32(loc[2])
            #     if loc[2] < 0:
            #         x = 0
            #     # z = np.int32(loc[0])
            #     # y = np.int32(loc[1])
            #     # x = np.int32(loc[2])
            #     landmark_array[z][y][x] = i + 1
            #
            # seq1 = iaa.Sequential([
            #     #iaa.Crop(px=(0, 16)),  # crop images from each side by 0 to 16px (randomly chosen)
            #     iaa.Fliplr(F),  # horizontally flip 50% of the images
            #     iaa.GaussianBlur(sigma=S),  # blur images with a sigma of 0 to 3.0
            #     #iaa.Dropout(p=0.03),
            #     iaa.SaltAndPepper(D),
            #     iaa.Affine(
            #         # scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}, # scale images to 80-120% of their size, individually per axis
            #         # translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)}, # translate by -20 to +20 percent (per axis)
            #         rotate=(R),  # rotate by -45 to +45 degrees
            #         # shear=(-16, 16), # shear by -16 to +16 degrees
            #         order=1,  # use nearest neighbour or bilinear interpolation (fast)
            #         # cval=(0, 4095), # if mode is constant, use a cval between 0 and 255
            #         mode=ia.ALL  # use any of scikit-image's warping modes (see 2nd image from the top for examples)
            #     )
            # ])
            #
            # seq2 = iaa.Sequential([
            #     # iaa.Crop(px=(0, 16)), # crop images from each side by 0 to 16px (randomly chosen)
            #     iaa.Fliplr(0.5), # horizontally flip 50% of the images
            #     # iaa.GaussianBlur(sigma=(0, 3.0)) # blur images with a sigma of 0 to 3.0
            #     # iaa.CoarseSaltAndPepper(0.02, size_percent=0.1),
            #     iaa.Affine(
            #         # scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}, # scale images to 80-120% of their size, individually per axis
            #         # translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)}, # translate by -20 to +20 percent (per axis)
            #         rotate=(R),  # rotate by -45 to +45 degrees
            #         # shear=(-16, 16), # shear by -16 to +16 degrees
            #         order=0,  # use nearest neighbour or bilinear interpolation (fast)
            #         # cval=(0, 255), # if mode is constant, use a cval between 0 and 255
            #         mode=ia.ALL  # use any of scikit-image's warping modes (see 2nd image from the top for examples)
            #     )
            # ])
            # images_aug_array = seq1.augment_images(padded_image)
            # mean = np.mean(images_aug_array)
            # std = np.std(images_aug_array)
            # padded_image1 = (images_aug_array - mean) / (std + 1e-6)
            # padded_image2 = (padded_image1 - padded_image1.min()) / (padded_image1.max() - padded_image1.min())
            # lmk_aug_array = seq2.augment_images(landmark_array)
            #
            # for i in range(26):
            #     loc = np.where(lmk_aug_array == i + 1)
            #     #print(loc[0][0],loc[1][0],loc[2][0])
            #     if len(loc[0]) == 0:
            #         continue
            #     else:
            #         z = int(loc[0][0])
            #         y = int(loc[1][0])
            #         x = int(loc[2][0])
            #         lmk_out[i][0] = z
            #         lmk_out[i][1] = y
            #         lmk_out[i][2] = x
            # mean = np.mean(padded_image)
            # std = np.std(padded_image)
            # padded_image1 = (padded_image - mean) / (std + 1e-6)
            # # padded_image2 = (padded_image1 - padded_image1.min()) / (padded_image1.max() - padded_image1.min())
            # if b == 0:
            #     batch_images = np.zeros((batch_size,) + padded_image1.shape, dtype=np.float32)
            #     batch_lmk = np.zeros((batch_size,) + lmk_1.shape, dtype=np.float32)
            # batch_images[b] = padded_image1.astype(np.float32)
            # image_in = np.expand_dims(batch_images, axis=0)
            # batch_lmk[b] = lmk_1 / padded_image_size
            # batch_class = np.expand_dims(class_id, axis=0)
            # # print(batch_class.shape)
            # inputs = [image_in, batch_class, np.expand_dims(t2, axis=0)]
            # outputs = []
            # # outputs = [batch_class,batch_lmk]
            # # print(lmk_out/padded_image_size)
            # yield inputs, outputs

    # while True:
    #         image_index = (image_index + 1) % len(train_or_val_list)
    #         if shuffle:
    #             np.random.shuffle(train_or_val_list)
    #         # Increment index to pick next image. Shuffle if at the start of an epoch.
    #         image_id = train_or_val_list[image_index]
    #         f = h5py.File(image_path + 'cropped_feature_{}.hdf5'.format(image_id - 1), 'r')
    #         image = f['feature'][()]
    #         f.close()
    #         # image = image.reshape(image.shape[0], -1)
    #         # image /= image.sum(1).reshape(-1, 1)
    #         image /= image.max()
    #         f = h5py.File(lmk_path + 'subject_{}.hdf5'.format(image_id), 'r')
    #         lmk = f['lmk'][()]
    #         f.close()
    #         lmk = lmk[0:26]
    #         # Init batch arrays
    #         #lmk /= lmk.max()
    #         A = np.ones((26,26))
    #         adj = sp.csr_matrix(A, dtype=np.float32)
    #         adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)
    #         T_k = preprocess_adj(adj, SYM_NORM)
    #         #print(T_k)
    #         # L = normalized_laplacian(adj, SYM_NORM)
    #         # L_scaled = rescale_laplacian(L)
    #         # T_k = chebyshev_polynomial(L_scaled, MAX_DEGREE)
    #
    #         if b == 0:
    #             batch_images = np.zeros((batch_size,) + image.shape, dtype=np.float32)
    #             batch_lmk = np.zeros((batch_size,) + lmk.shape, dtype=np.float32)
    #             batch_adj = np.zeros((batch_size,) + T_k.shape, dtype=np.float32)
    #         batch_images[b] = image.astype(np.float32)
    #         batch_lmk[b] = lmk
    #
    #         inputs = batch_images
    #         outputs = batch_lmk
    #
    #         yield inputs, outputs

def lr_schedule(epoch):
    if epoch in [0,1,2,3,4,5,6,7,8,9,10]:
        lr = 0.01
        print('Learning rate of epoch {0} is {1}'.format(epoch + 1, lr))
        return lr
    elif epoch in[11,12,13,14,15,16,17,18,19,20]:
        lr = 0.005
        print('Learning rate of epoch {0} is {1}'.format(epoch + 1, lr))
        return lr
    elif epoch in [21, 22,23, 24,25,26,27,28,29,30]:
        lr = 0.002
        print('Learning rate of epoch {0} is {1}'.format(epoch + 1, lr))
        return lr
    elif epoch in [31,32,33,34,35,36,37,38,39,40]:
        lr = 0.0005
        print('Learning rate of epoch {0} is {1}'.format(epoch + 1, lr))
        return lr
    else:
        print('Learning rate of this epoch is {0}'.format(1e-5))
        return 0.0001