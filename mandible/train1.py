from config import Config
import model as modellib
import numpy as np
import utils
import utils
import module
from keras.models import Model
from keras.layers import Input, Lambda, Concatenate, Add, TimeDistributed
from keras.regularizers import l2
from keras.callbacks import LearningRateScheduler, ReduceLROnPlateau, ModelCheckpoint, TensorBoard, Callback
from keras.layers.core import Dense, Activation, Dropout, Flatten, Reshape
from keras.layers.convolutional import Conv3D, UpSampling3D, ZeroPadding3D
import keras.layers as KL
import keras.engine as KE
import keras.models as KM
import h5py
from keras.layers.pooling import MaxPooling3D, AveragePooling3D
from keras.layers.normalization import BatchNormalization
from keras.layers.merge import concatenate
from keras.optimizers import SGD, RMSprop, Adadelta, Adam
from keras import backend as K
import tensorflow as tf


model = modellib.MaskRCNN(mode="training", config=Config,
                                  model_dir='/home/yankun/tensorflow')

train_list1 = np.arange(1,151)
# train_list2 = np.arange(91,155)
# train_list = np.concatenate((train_list1, train_list2))
# print(train_list.shape)
validation_list = np.arange(150,200)
learning_rate = 0.01
epoch = 20
model.train(train_list1, validation_list, learning_rate, epoch)
print('done')
# if __name__ == '__main__':
#     print('111')
#     train()