import numpy as np
import h5py
import os
import time
import sys
from sys import getsizeof
import numpy as np
import h5py
import time
import scipy.io as sio
import SimpleITK as sitk
import scipy.ndimage as nd
from scipy.ndimage.filters import gaussian_filter
from scipy import ndimage
#from config import Config



def main():
    def read_landmark(filename, file_index):
        directory = current_directory + '/landmarks_bone_selected_index/'
        if not os.path.exists(directory):
            os.makedirs(directory)
        save_filename = 'subject_{}.hdf5'.format(file_index)
        #lmk_index = np.arange(0,105)
        lmk_index = [0,2,3,4,5,8,9,12,13,14,15,16,17,18,19,20,22,23,24,25,29,30,
                     31,32,33,34,36,37,38,39,40,41,42,43,44,45,46,48,49,50,51,52,
                     53,58,59,60,61,62,63,64,65,66,67,68,69,70,71,74,75,76,77,78,
                     79,80,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,
                     101,102,103,104,105,111,113,118,119,120,121,122,123,124,125,126,
                     127,130,131,132,133,134,135]
        lmks = np.zeros((138, 3), dtype=np.float32)
        id = 0
        print(filename)
        with open(filename, 'r') as f:
            for line in f:
                if len(line) < 5:
                    continue
                else:
                    words = line.split()
                    # print(words[0], words[1], words[2])
                    lmks[id][0] = words[0]
                    lmks[id][1] = words[1]
                    lmks[id][2] = words[2]
                    id += 1
        lmk_105 = lmks[lmk_index]
        f = h5py.File(directory + save_filename, 'w')
        f['lmk'] = lmk_105
        #print(lmk_105.shape)
        f.close()
        print("landmark_{}_done".format(file_index))
        return lmk_105

    def read_image(file_image, file_mandible, file_midface, lmks, file_index):
        image = sitk.ReadImage(file_image)
        spacing = image.GetSpacing()
        image_array = sitk.GetArrayFromImage(image)
        sizeofimage = np.shape(image_array)
        image_seg_man = sitk.ReadImage(file_mandible)
        seg_man_array = sitk.GetArrayFromImage(image_seg_man)
        seg_man_array[seg_man_array > 0] = 125
        image_seg_mid = sitk.ReadImage(file_midface)
        seg_mid_array = sitk.GetArrayFromImage(image_seg_mid)
        seg_mid_array[seg_mid_array > 0] = 255
        seg_array = seg_man_array + seg_mid_array
        #print(lmks.shape)
        #print(np.array(spacing).shape)
        lmks = lmks/np.array(spacing)
        norm_ratio = spacing / common_spacing
        #print(norm_ratio)
        #print(lmks[0])
        ds_lmk = norm_ratio * lmks
        #print(ds_lmk[0])
        ds_lmk = ds_lmk[:, ::-1]  # trans from (x,y,z) to (z,y,x)
        norm_ratio = norm_ratio[::-1]
        #################### change resolution ############################
        SN_image_array = nd.interpolation.zoom(image_array, norm_ratio, order=1)
        ds_seg = nd.interpolation.zoom(seg_array, zoom=norm_ratio, order=0)
        # ds_man = nd.interpolation.zoom(seg_man_array, zoom=norm_ratio, order=0)
        # ds_mid = nd.interpolation.zoom(seg_mid_array, zoom=norm_ratio, order=0)
        #
        # SN_image = sitk.GetImageFromArray(SN_image_array)
        # SN_image.SetSpacing(common_spacing)
        #
        # SN_seg = sitk.GetImageFromArray(ds_seg)
        # SN_seg.SetSpacing(common_spacing)
        # SN_man = sitk.GetImageFromArray(ds_man)
        # SN_man.SetSpacing(common_spacing)
        # SN_mid = sitk.GetImageFromArray(ds_mid)
        # SN_mid.SetSpacing(common_spacing)
        # sitk.WriteImage(SN_man,
        #                 '/home/yankun/CBCT patient image 105/data_lang/0.4mm/segmentation/man_{0}.nii.gz'.format(
        #                     file_index))
        # sitk.WriteImage(SN_mid,
        #                 '/home/yankun/CBCT patient image 105/data_lang/0.4mm/segmentation/mid_{0}.nii.gz'.format(
        #                     file_index))
        return SN_image_array, ds_seg, ds_lmk

    def rescale(image):
        minimum = np.min(image)
        maximum = np.max(image)
        #print(minimum, maximum)
        intensity_range = maximum - minimum
        scale = intensity_range / (1.0 * 4095)
        scaled_image_array = (np.floor((image - minimum) / scale)).astype(np.float32)
        return scaled_image_array

    def histogram(image_match, image, save_path, file_index):
        R = sitk.GetImageFromArray(image_match)
        R.SetSpacing(common_spacing)
        I = sitk.GetImageFromArray(image)
        I.SetSpacing(common_spacing)
        NI = sitk.HistogramMatching(I, R)
        image_array = sitk.GetArrayFromImage(NI)
        sitk.WriteImage(NI, save_path + 'HM_Rescaled_subject_{0}.nii.gz'.format(file_index))
        f = h5py.File(RC_HM_save_path + 'HM_Rescaled_subject_{0}.hdf5'.format(file_index), 'w')
        f['image'] = image_array
        f.close()
    # def sanity(image, segmentaiton, lmk, sanity_path):
    #     numoflmk = lmk.shape[0]
    #     for i in range(numoflmk):
    #         z = np.int32(lmk[i][2])
    #         y = np.int32(lmk[i][1])
    #         x = np.int32(lmk[i][0])
    #         #print(x,y,z)
    #         #print(segmentation.shape)
    #         segmentaiton[x - 2:x + 2, y - 2:y + 2, z - 2:z + 2] = i + 1
    #     SN_seg_vis = sitk.GetImageFromArray(segmentaiton)
    #     SN_seg_vis.SetSpacing(common_spacing)
    #     sitk.WriteImage(SN_seg_vis, sanity_path)


    arg = sys.argv[1]
    #print(arg)
    res = float(arg)
    #print(res)
    tt = sys.argv[2]
    #print(tt)
    current_directory = os.getcwd()
    #data_directory = sys.argv[3]
    folder_name = current_directory + '/data' #+ data_directory #'/home/yankun/data/'  # change to your files' original path
    subject_list = np.arange(1,100) #Config.subject_list
    common_spacing = np.array([res, res, res])  # change to 0.4 or 1.6 for other resolution
    resolution = arg + 'mm/'
    for i in range(len(subject_list)):
        file_index = subject_list[i]
        subfolder = '/'
        filename_lmk = 'patient_{:02}_landmarks_bone.txt'.format(file_index)
        filename_image = 'patient_{:02}_origin.nii.gz'.format(file_index)
        filename_midface = 'patient_{:02}_midface.nii.gz'.format(file_index)
        filename_mandible = 'patient_{:02}_mandible.nii.gz'.format(file_index)
        file_lmk = folder_name + subfolder + filename_lmk
        file_image = folder_name + subfolder + filename_image
        file_midface = folder_name + subfolder + filename_midface
        file_mandible = folder_name + subfolder + filename_mandible
        if not os.path.exists(file_image):
            continue
        else:
            #################### read original landmarks ###########################
            lmks = read_landmark(file_lmk, file_index)
            #################### read and down-sample image/segmentation/lmk #######
            image, segmentation, lmk_downsampled = read_image(file_image, file_mandible, file_midface, lmks, file_index)

            #################### save DSed lmk #####################################
            directory = current_directory + '/' + tt + '/' + resolution + 'landmarks/'
            if not os.path.exists(directory):
                os.makedirs(directory)
            lmk_savename = directory + 'subject_{}.hdf5'.format(file_index)
            f = h5py.File(lmk_savename, 'w')
            f['lmk'] = lmk_downsampled
            f.close()
            # print(lmk_downsampled)
            #################### image proprecessing ###############################
            image_rescale = rescale(image)
            #################### image histogram matching ##########################
            RC_HM_save_path = current_directory + '/' + tt + '/' + resolution + 'RC_HM_image/'
            segmentation_path = current_directory + '/' + tt + '/' + resolution + 'segmentation/'
            if not os.path.exists(RC_HM_save_path):
                os.makedirs(RC_HM_save_path)
            if res is 0.4:
                f = h5py.File('HM_Rescaled_subject_04.hdf5', 'r')
                image_match = f['image']
                histogram(image_match, image_rescale, RC_HM_save_path, file_index)
            else:
                f = h5py.File('HM_Rescaled_subject_1.hdf5', 'r')
                image_match = f['image']
                histogram(image_match, image_rescale, RC_HM_save_path, file_index)

            #################### save segmentation ################################
            if not os.path.exists(segmentation_path):
                os.makedirs(segmentation_path)
            Seg = sitk.GetImageFromArray(segmentation)
            Seg.SetSpacing(common_spacing)
            sitk.WriteImage(Seg, segmentation_path + 'segmentation_{0}.nii.gz'.format(file_index))
            f = h5py.File(segmentation_path + 'Seg_subject_{0}.hdf5'.format(file_index), 'w')
            f['segmentation'] = segmentation
            f.close()
            # sanity_path = current_directory + '/' + tt + '/' + resolution + 'sanity/'
            # if not os.path.exists(sanity_path):
            #     os.makedirs(sanity_path)
            # sanity(image_rescale, segmentation, lmk_downsampled, sanity_path + 'sanity_{0}.nii.gz'.format(file_index))
            ############################################################################
            if res > 0.5:
                for j in range(1, 3):
                    if j is 1:
                        num = 33
                        bias = 0
                        save = '/0-33'
                    # elif j is 2:
                    #     num = 30
                    #     bias = 30
                    #     save = '/30-60'
                    else:
                        num = 27
                        bias = 58
                        save = '/58-85'
                    W = np.ones((3, 3, 3), np.float32) / 27.
                    ts = lmk_downsampled.shape
                    t2 = np.zeros((num, image.shape[0], image.shape[1], image.shape[2]), np.float32)
                    # print(t2.shape[0], image.shape)
                    #print(lmk_downsampled)
                    for i in range(0, t2.shape[0]):
                        x1 = int(lmk_downsampled[i + bias][0])
                        y1 = int(lmk_downsampled[i + bias][1])
                        z1 = int(lmk_downsampled[i + bias][2])
                        # print(x1,y1,z1)
                        # print(t2.shape)
                        # print(i)
                        if x1 == 0:
                            continue
                        else:
                            if x1 >= image.shape[0] - 1:
                                continue
                            else:
                                t2[i][x1][y1][z1] = 10000.

                                for k in range(5):
                                    t2[i] = ndimage.convolve(t2[i], W, mode='constant', cval=0.0)
                    h_msave_path = current_directory + '/train/1.6mm/heatmap' + save
                    if not os.path.exists(h_msave_path):
                        os.makedirs(h_msave_path)
                    f = h5py.File(
                        h_msave_path + '/subject_{0}.hdf5'.format(file_index), 'w')
                    f['heatmaps'] = t2
                    f.close()

            print("done_{}".format(file_index))


if __name__ == "__main__":
    main()