FROM tensorflow/tensorflow:1.7.0-devel-gpu-py3

WORKDIR /user/package
COPY data_preprocessing.py /user/package/data_preprocessing.py

COPY data_preprocessing.sh /user/package/data_preprocessing.sh
COPY train.sh /user/package/train.sh
COPY . .
COPY HM_Rescaled_subject_1.hdf5 /user/package/HM_Rescaled_subject_1.hdf5


RUN pip3 install wheel
RUN pip3 install scipy
RUN pip3 install numpy
RUN pip3 install SimpleITK
RUN pip3 install h5py
RUN pip3 install keras==2.1.5

