import numpy as np
import tensorflow as tf
import utils
from config import Config
import tensorflow as tf
import keras
import keras.backend as K
import keras.layers as KL
import keras.engine as KE
import keras.models as KM
from keras.models import Model
from keras.layers import Input, Lambda, Concatenate, Add, TimeDistributed
from keras.regularizers import l2
from keras.callbacks import LearningRateScheduler, ReduceLROnPlateau, ModelCheckpoint, TensorBoard, Callback
from keras.layers.core import Dense, Activation, Dropout, Flatten, Reshape
from keras.layers.convolutional import Conv3D, UpSampling3D, ZeroPadding3D
from keras.layers import Input, Lambda, Concatenate, Add, TimeDistributed
from keras.regularizers import l2
from keras.callbacks import LearningRateScheduler, ReduceLROnPlateau, ModelCheckpoint, TensorBoard, Callback
from keras.layers.core import Dense, Activation, Dropout, Flatten, Reshape
from keras.layers.convolutional import Conv3D, UpSampling3D, ZeroPadding3D
from keras.layers.pooling import MaxPooling3D, AveragePooling3D
from keras.layers.normalization import BatchNormalization
from keras.layers.merge import concatenate
from keras.optimizers import SGD, RMSprop, Adadelta, Adam
from keras import initializers
from keras import regularizers
from keras import constraints
from keras.engine import Layer, InputSpec
from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import sparse_ops

######################################################################
# Region Proposal Network (RPN)
######################################################################
def rpn_graph(feature_map, num_of_lmk):
    """Builds the computation graph of Region Proposal Network.

    feature_map: backbone features [batch, height, width, depth]
    anchors_per_location: number of anchors per pixel in the feature map
    anchor_stride: Controls the density of anchors. Typically 1 (anchors for
                   every pixel in the feature map), or 2 (every other pixel).

    Returns:
        rpn_logits: [batch, H, W, D, 2] Anchor classifier logits (before softmax)
        rpn_probs: [batch, H, W, D, 2] Anchor classifier probabilities.
        rpn_bbox: [batch, H, W, D, (dz, dy, dx, log(dd), log(dh), log(dw))] Deltas to be
                  applied to anchors.
    """
    # TODO: check if stride of 2 causes alignment issues if the featuremap
    #       is not even.
    # Shared convolutional base of the RPN
    output = Conv3D(num_of_lmk, (1, 1, 1), padding='same', data_format='channels_first',
                    kernel_initializer='he_normal',name='heatmap')(feature_map)
    #output = Activation('relu')(output)

    # shared = Conv3D(512, (3, 3, 3), padding='same', data_format='channels_first', name='rpn_conv_shared')(feature_map)
    # shared = Activation('relu')(shared)
    #
    #
    # # Anchor Score. [batch, height, width, anchors per location * 2].
    # x = Conv3D(num_of_lmk, (1, 1, 1), padding='valid', activation='relu', data_format='channels_first',
    #             name='rpn_class_raw')(shared)
    # #x = Lambda(lambda x: K.permute_dimensions(x, (0, 2, 3, 4, 1)))(shared)
    # # Reshape to [batch, anchors, 2]
    # x = KL.Lambda(
    #     lambda t: tf.reshape(t, [tf.shape(t)[0], num_of_lmk, 900]))(x)
    # x1 = KL.TimeDistributed(KL.Dense(512, activation='relu', kernel_initializer='he_normal'))(x)
    # x1 = KL.TimeDistributed(KL.Dense(256, activation='relu', kernel_initializer='he_normal'))(x1)
    # rpn_class_logits = KL.TimeDistributed(KL.Dense(2))(x1)
    # print(rpn_class_logits)
    # # Softmax on last dimension of BG/FG.
    # rpn_probs = KL.Activation(
    #     "softmax", name="rpn_class_xxx")(rpn_class_logits)
    #
    # # Bounding box refinement. [batch, H, W, anchors per location, depth]
    # # where depth is [x, y, log(w), log(h)]
    # x = Conv3D(num_of_lmk, (1, 1, 1), padding="valid", activation='relu', data_format='channels_first',name='rpn_bbox_pred')(shared)
    # #x = Lambda(lambda x: K.permute_dimensions(x, (0, 2, 3, 4, 1)))(x)
    # # Reshape to [batch, anchors, 6]
    # x = KL.Lambda(lambda t: tf.reshape(t, [tf.shape(t)[0], num_of_lmk, 900]))(x)
    # x2 = KL.TimeDistributed(KL.Dense(1024, activation='relu', kernel_initializer='he_normal'))(x)
    #
    # x2 = KL.TimeDistributed(KL.Dense(512, activation='relu', kernel_initializer='he_normal'))(x)
    # #x2 = Dropout(0.5)(x2)
    #
    # x2 = KL.TimeDistributed(KL.Dense(256, activation='relu', kernel_initializer='he_normal'))(x2)
    # #x2 = Dropout(0.5)(x2)
    #
    # x2 = KL.TimeDistributed(KL.Dense(128, activation='relu', kernel_initializer='he_normal'))(x2)
    # #x2 = Dropout(0.5)(x2)
    #
    # x2 = KL.TimeDistributed(KL.Dense(64, activation='relu', kernel_initializer='he_normal'))(x2)
    # #x2 = Dropout(0.5)(x2)
    #
    # x2 = KL.TimeDistributed(KL.Dense(32, activation='relu', kernel_initializer='he_normal'))(x2)
    # x2 = Dropout(0.5)(x2)
    #
    # x2 = KL.TimeDistributed(KL.Dense(16, activation='relu', kernel_initializer='he_normal'))(x2)
    #
    # rpn_bbox = KL.TimeDistributed(KL.Dense(3))(x2)
    # print(rpn_bbox)
    # #print(tf.shape(rpn_bbox)[1])
    return output


class build_rpn_model:
    #(anchor_stride, anchors_per_location):
    """Builds a Keras model of the Region Proposal Network.
    It wraps the RPN graph so it can be used multiple times with shared
    weights.

    anchors_per_location: number of anchors per pixel in the feature map
    anchor_stride: Controls the density of anchors. Typically 1 (anchors for
                   every pixel in the feature map), or 2 (every other pixel).
    depth: Depth of the backbone feature map.

    Returns a Keras Model object. The model outputs, when called, are:
    rpn_logits: [batch, H, W, 2] Anchor classifier logits (before softmax)
    rpn_probs: [batch, W, W, 2] Anchor classifier probabilities.
    rpn_bbox: [batch, H, W, (dy, dx, log(dh), log(dw))] Deltas to be
                applied to anchors.
    """
    def __init__(self, num_of_lmk):
        self.num_of_lmk = num_of_lmk

    def __call__(self, input_feature_map):
        outputs = rpn_graph(input_feature_map, self.num_of_lmk)
        return outputs
    # input_feature_map = KL.Input(shape=[32, None, None, None],
    #                              name="input_rpn_feature_map")
    # outputs = rpn_graph(input_feature_map, anchors_per_location, anchor_stride)
    # return KM.Model([input_feature_map], outputs, name="rpn_model")

######################################################################
# Detection Target Layer
######################################################################
def detection_keypoint_targets_graph(proposals, gt_class_ids, gt_boxes, gt_keypoints, gt_masks, config):
    """Generates detection targets for one image. Subsamples proposals and
    generates target class IDs, bounding box deltas, and masks for each.
    Inputs:
    proposals: [N, (y1, x1, y2, x2)] in normalized coordinates. Might
               be zero padded if there are not enough proposals.
    gt_class_ids: [MAX_GT_INSTANCES] int class IDs
    gt_boxes: [MAX_GT_INSTANCES, (y1, x1, y2, x2)] in normalized coordinates.
    gt_keypoints: [MAX_GT_INSTANCES, NUM_KEYPOINTS, 3] of (x, y ,v)
    gt_masks: [height, width, MAX_GT_INSTANCES] of boolean type.
    Returns: Target ROIs and corresponding class IDs, bounding box shifts, keypoint label, keypoint weight
    and masks.
    rois: [TRAIN_ROIS_PER_IMAGE, (y1, x1, y2, x2)] in normalized coordinates
    class_ids: [TRAIN_ROIS_PER_IMAGE]. Integer class IDs. Zero padded.
    deltas: [TRAIN_ROIS_PER_IMAGE, NUM_CLASSES, (dy, dx, log(dh), log(dw))]
            Class-specific bbox refinements.
    keypoints_labels: [TRAIN_ROIS_PER_IMAGE, NUM_KEYPOINTS). Keypoint labels in [0, HEATMAP_SIZE-1]
    HEATMAP_SIZE = HEAT_MAP_WITHD * HEAT_MAP_HEIGHT
    keypoints_weights: [TRAIN_ROIS_PER_IMAGE, NUM_KEYPOINTS), 0: not visible 1: visible
    Note: Returned arrays might be zero padded if not enough target ROIs.
    """
    # Assertions
    asserts = [
        tf.Assert(tf.greater(tf.shape(proposals)[0], 0), [proposals],
                  name="roi_assertion"),
    ]
    with tf.control_dependencies(asserts):
        proposals = tf.identity(proposals)

    # Remove zero padding
    proposals, _ = utils.trim_zeros_graph(proposals, name="trim_proposals")
    #non_zeros:[N_box,1] true false
    gt_boxes, non_zeros = utils.trim_zeros_graph(gt_boxes, name="trim_gt_boxes")
    gt_class_ids = tf.boolean_mask(gt_class_ids, non_zeros,
                                   name="trim_gt_class_ids")
    gt_keypoints = tf.gather(gt_keypoints, tf.where(non_zeros)[:, 0], axis=0,
                         name="trim_gt_keypoints")
    gt_masks = tf.gather(gt_masks, tf.where(non_zeros)[:, 0], axis=3,
                         name="trim_gt_masks")


    # Compute overlaps matrix [proposals, gt_boxes]
    overlaps = utils.overlaps_graph(proposals, gt_boxes)



    # Determine postive and negative ROIs
    roi_iou_max = tf.reduce_max(overlaps, axis=1)
    # 1. Positive ROIs are those with >= 0.5 IoU with a GT box
    positive_roi_bool = (roi_iou_max >= 0.2)
    positive_indices = tf.where(positive_roi_bool)[:, 0]
    # 2. Negative ROIs are those with < 0.5 with every GT box. Skip crowds.
    negative_indices = tf.where(roi_iou_max < 0.2)[:, 0]

    # Subsample ROIs. Aim for 33% positive
    # Positive ROIs
    positive_count = int(config.TRAIN_ROIS_PER_IMAGE *
                         config.ROI_POSITIVE_RATIO)
    positive_indices = tf.random_shuffle(positive_indices)[:positive_count]

    r = 1.0 / 0.3
    negative_count = config.TRAIN_ROIS_PER_IMAGE - positive_count

    negative_indices = tf.random_shuffle(negative_indices)[:negative_count]
    # else:
    #    negative_indices = negative_indices
    # Gather selected ROIs
    positive_rois = tf.gather(proposals, positive_indices)
    negative_rois = tf.gather(proposals, negative_indices)

    # Assign positive ROIs to GT boxes.
    positive_overlaps = tf.gather(overlaps, positive_indices)
    roi_gt_box_assignment = tf.argmax(positive_overlaps, axis=1)
    roi_gt_boxes = tf.gather(gt_boxes, roi_gt_box_assignment)
    roi_gt_class_ids = tf.gather(gt_class_ids, roi_gt_box_assignment)

    # Compute bbox refinement for positive ROIs
    deltas = utils.box_refinement_graph(positive_rois, roi_gt_boxes)
    deltas /= config.BBOX_STD_DEV

    # Assign positive ROIs to GT masks

    # Permute masks to [N, height, width, 1]
    transposed_masks = tf.expand_dims(tf.transpose(gt_masks, [3, 0, 1, 2]), -1)
    # Pick the right mask for each ROI
    roi_keypoints = tf.gather(gt_keypoints, roi_gt_box_assignment)
    roi_masks = tf.gather(transposed_masks, roi_gt_box_assignment)
    # Compute mask targets
    # Compute mask targets
    boxes = positive_rois
    box_ids = tf.range(0, tf.shape(roi_masks)[0])
    box_opt = tf.load_op_library('/home/yankun/tensorflow/CR1/crop_and_resize_op_gpu.so')
    masks = box_opt.crop_resize(tf.cast(roi_masks, tf.float32), boxes,
                                box_ids,
                                [32, 32, 32])
    # Remove the extra dimension from masks.
    masks = tf.squeeze(masks, axis=4)
    masks = tf.round(masks)

    ## Transform ROI keypoints from (x,y) image space to keypoint label
    z1, y1, x1, z2, y2, x2 = tf.split(positive_rois, 6, axis=1)
    z1 = z1[:, 0]
    y1 = y1[:, 0]
    x1 = x1[:, 0]
    z2 = z2[:, 0]
    y2 = y2[:, 0]
    x2 = x2[:, 0]
    scale_z = tf.cast(config.MASK_SHAPE[0] / ((z2 - z1) * config.IMAGE_SHAPE[0]), tf.float32)
    scale_x = tf.cast(config.MASK_SHAPE[2] / ((x2 - x1) * config.IMAGE_SHAPE[2]), tf.float32)
    scale_y = tf.cast(config.MASK_SHAPE[1] / ((y2 - y1) * config.IMAGE_SHAPE[1]), tf.float32)
    keypoint_lables = []


    for k in range(config.NUM_KEYPOINTS):
        #vis = roi_keypoints[:, k, 2] > 0
        x = tf.cast(roi_keypoints[:, k, 2], tf.float32)
        y = tf.cast(roi_keypoints[:, k, 1], tf.float32)
        z = tf.cast(roi_keypoints[:, k, 0], tf.float32)

        # # recover from normlized corrdinates to real wordl
        x_real = (x - x1) * config.IMAGE_SHAPE[2]
        y_real = (y - y1) * config.IMAGE_SHAPE[1]
        z_real = (z - z1) * config.IMAGE_SHAPE[0]
        ## transform the box size into feature map size
        z_real_map = tf.cast(z_real * scale_z + 0.5, tf.int32)
        y_real_map = tf.cast(y_real * scale_y + 0.5, tf.int32)
        x_real_map = tf.cast(x_real * scale_x + 0.5, tf.int32)

        z_boundary_bool = tf.cast((z_real_map == config.MASK_SHAPE[0]), tf.int32)
        y_boundary_bool = tf.cast((y_real_map == config.MASK_SHAPE[1]), tf.int32)
        x_boundary_bool = tf.cast((x_real_map == config.MASK_SHAPE[2]), tf.int32)

        z_real_map = z_real_map * (1 - z_boundary_bool) + z_boundary_bool * (config.MASK_SHAPE[0] - 1)
        y_real_map = y_real_map * (1 - y_boundary_bool) + y_boundary_bool * (config.MASK_SHAPE[1] - 1)
        x_real_map = x_real_map * (1 - x_boundary_bool) + x_boundary_bool * (config.MASK_SHAPE[2] - 1)

        valid_loc = tf.logical_and(tf.logical_and(
            tf.logical_and(x_real_map > 0, x_real_map < config.MASK_SHAPE[2]),
            tf.logical_and(y_real_map > 0, y_real_map < config.MASK_SHAPE[1])),
            tf.logical_and(z_real_map > 0, z_real_map < config.MASK_SHAPE[0]),

        )
        #valid = tf.logical_and(valid_loc, vis)
        #keypoint_weights.append(valid)

        valid = tf.cast(valid_loc, tf.int32)
        z_real_map = z_real_map * tf.cast(valid, tf.int32)
        y_real_map = y_real_map * tf.cast(valid, tf.int32)
        x_real_map = x_real_map * tf.cast(valid, tf.int32)

        #calculate the keypoint label betwween[0, map_h*map_w)
        keypoint_label = z_real_map * (config.MASK_SHAPE[1] * config.MASK_SHAPE[2]) + y_real_map * config.MASK_SHAPE[2] + x_real_map
        keypoint_label = tf.expand_dims(keypoint_label, -1)
        keypoint_lables.append(keypoint_label)

    # shape:[N_roi, num_keypoint]
    keypoint_lables = tf.cast(tf.concat(keypoint_lables, axis=1), tf.int32)

    rois = tf.concat([positive_rois, negative_rois], axis=0)
    N = tf.shape(negative_rois)[0]
    P = tf.maximum(config.TRAIN_ROIS_PER_IMAGE - tf.shape(rois)[0], 0)
    rois = tf.pad(rois, [(0, P), (0, 0)])
    roi_gt_class_ids = tf.pad(roi_gt_class_ids, [(0, N + P)])
    deltas = tf.pad(deltas, [(0, N + P), (0, 0)])
    keypoint_lables = tf.pad(keypoint_lables, [(0, N + P), (0, 0)])
    masks = tf.pad(masks, [[0, N + P], (0, 0), (0, 0), (0, 0)])

    return rois, roi_gt_class_ids, deltas, keypoint_lables, masks



class DetectionKeypointTargetLayer(KE.Layer):
    """Subsamples proposals and generates target box refinement, class_ids,keypoint_weights
    and keypoint_masks for each.
    Inputs:
    proposals: [batch, N, (y1, x1, y2, x2)] in normalized coordinates. Might
               be zero padded if there are not enough proposals, Here N <= RPN_TRAIN_ANCHORS_PER_IMAGE(256)
               because of the NMS e.t.c
    gt_class_ids: [batch, MAX_GT_INSTANCES] Integer class IDs.
    gt_boxes: [batch, MAX_GT_INSTANCES, (y1, x1, y2, x2)] in normalized
              coordinates.
    gt_keypoints: [batch, MAX_GT_INSTANCES, NUM_KEYPOINTS, 3]
                (x, y, v)
    Returns: Target ROIs and corresponding class IDs, bounding box shifts,
    keypoint_weights and keypoint_masks.
    rois: [batch, TRAIN_ROIS_PER_IMAGE, (y1, x1, y2, x2)] in normalized
          coordinates
    target_class_ids: [batch, TRAIN_ROIS_PER_IMAGE]. Integer class IDs.
    target_deltas: [batch, TRAIN_ROIS_PER_IMAGE, NUM_CLASSES,
                    (dy, dx, log(dh), log(dw), class_id)]
                   Class-specific bbox refinements.
    target_keypoints: [batch, TRAIN_ROIS_PER_IMAGE, NUM_KEYPOINTS)
                 Keypoint labels cropped to bbox boundaries and resized to neural
                 network output size. Maps keypoints from the half-open interval [x1, x2) on continuous image
                coordinates to the closed interval [0, HEATMAP_SIZE - 1]
    target_keypoint_weights: [batch, TRAIN_ROIS_PER_IMAGE, NUM_KEYPOINTS), bool type
                 Keypoint_weights, 0: isn't visible, 1: visilble
    Note: Returned arrays might be zero padded if not enough target ROIs.
    """

    def __init__(self, config, **kwargs):
        super(DetectionKeypointTargetLayer, self).__init__(**kwargs)
        self.config = config

    def call(self, inputs):
        proposals = inputs[0]
        gt_class_ids = inputs[1]
        gt_boxes = inputs[2]
        gt_keypoints = inputs[3]
        gt_masks = inputs[4]

        # Slice the batch and run a graph for each slice
        # TODO: Rename target_bbox to target_deltas for clarity
        names = ["rois", "target_class_ids", "target_bbox", "target_keypoint","target_mask"]
        outputs = utils.batch_slice(
            [proposals, gt_class_ids, gt_boxes, gt_keypoints, gt_masks],
            lambda r, x, y, z, m: detection_keypoint_targets_graph(
                r, x, y, z, m,self.config),
            1, names=names)
        return outputs

    def compute_output_shape(self, input_shape):
        return [
            (None, self.config.TRAIN_ROIS_PER_IMAGE, 6),  # rois
            (None, 1),  # class_ids
            (None, self.config.TRAIN_ROIS_PER_IMAGE, 6),  # deltas
            (None, self.config.TRAIN_ROIS_PER_IMAGE,self.config.NUM_KEYPOINTS) , # keypoint_labels
            (None, self.config.TRAIN_ROIS_PER_IMAGE, 32,
             32,32)  # masks
]
def detection_targets_graph(proposals, gt_class_ids, gt_boxes, gt_masks,config):
    """Generates detection targets for one image. Subsamples proposals and
    generates target class IDs, bounding box deltas, and masks for each.

    Inputs:
    proposals: [N, (z1, y1, x1, z2, y2, x2)] in normalized coordinates. Might
               be zero padded if there are not enough proposals.
    gt_class_ids: [MAX_GT_INSTANCES] int class IDs
    gt_boxes: [MAX_GT_INSTANCES, (z1, y1, x1, z2, y2, x2)] in normalized coordinates.
    gt_masks: [height, width, depth, MAX_GT_INSTANCES] of boolean type.

    Returns: Target ROIs and corresponding class IDs, bounding box shifts,
    and masks.
    rois: [TRAIN_ROIS_PER_IMAGE, (z1, y1, x1, z2, y2, x2)] in normalized coordinates
    class_ids: [TRAIN_ROIS_PER_IMAGE]. Integer class IDs. Zero padded.
    deltas: [TRAIN_ROIS_PER_IMAGE, NUM_CLASSES, (dz, dy, dx, log(dd), log(dh), log(dw))]
            Class-specific bbox refinements.
    masks: [TRAIN_ROIS_PER_IMAGE, height, width, depth). Masks cropped to bbox
           boundaries and resized to neural network output size.

    Note: Returned arrays might be zero padded if not enough target ROIs.
    """
    #print(gt_masks.shape)
    # Assertions
    asserts = [
        tf.Assert(tf.greater(tf.shape(proposals)[0], 0), [proposals],
                  name="roi_assertion"),
    ]
    with tf.control_dependencies(asserts):
        proposals = tf.identity(proposals)

    # Remove zero padding
    proposals, _ = utils.trim_zeros_graph(proposals, name="trim_proposals")
    gt_boxes, non_zeros = utils.trim_zeros_graph(gt_boxes, name="trim_gt_boxes")
    gt_class_ids = tf.boolean_mask(gt_class_ids, non_zeros,
                                   name="trim_gt_class_ids")
    gt_masks = tf.gather(gt_masks, tf.where(non_zeros)[:, 0], axis=3,
                         name="trim_gt_masks")

    # # # Handle COCO crowds
    # # # A crowd box in COCO is a bounding box around several instances. Exclude
    # # # them from training. A crowd box is given a negative class ID.
    # crowd_ix = tf.where(gt_class_ids < 0)[:, 0]
    # non_crowd_ix = tf.where(gt_class_ids > 0)[:, 0]
    # crowd_boxes = tf.gather(gt_boxes, crowd_ix)
    # crowd_masks = tf.gather(gt_masks, crowd_ix, axis=3)
    # gt_class_ids = tf.gather(gt_class_ids, non_crowd_ix)
    # gt_boxes = tf.gather(gt_boxes, non_crowd_ix)
    # gt_masks = tf.gather(gt_masks, non_crowd_ix, axis=3)


    # Compute overlaps matrix [proposals, gt_boxes]
    overlaps = utils.overlaps_graph(proposals, gt_boxes)

    # Compute overlaps with crowd boxes [anchors, crowds]
    # crowd_overlaps = utils.overlaps_graph(proposals, crowd_boxes)
    # crowd_iou_max = tf.reduce_max(crowd_overlaps, axis=1)
    # no_crowd_bool = (crowd_iou_max < 0.001)
    # Determine postive and negative ROIs
    roi_iou_max = tf.reduce_max(overlaps, axis=1)

    # 1. Positive ROIs are those with >= 0.5 IoU with a GT box
    positive_roi_bool = (roi_iou_max >= 0.2)

    positive_indices = tf.where(positive_roi_bool)[:, 0]

    # 2. Negative ROIs are those with < 0.5 with every GT box. Skip crowds.
    #negative_indices = tf.where(tf.logical_and(roi_iou_max < 0.5, no_crowd_bool))[:, 0]
    negative_indices = tf.where(roi_iou_max < 0.2)[:, 0]

    #return negative_indices
    # Subsample ROIs. Aim for 33% positive
    # Positive ROIs
    positive_count = int(config.TRAIN_ROIS_PER_IMAGE *
                         config.ROI_POSITIVE_RATIO)
    #if tf.shape(positive_indices)[0] > positive_count:
    positive_indices = tf.random_shuffle(positive_indices)[:positive_count]
    # else:
    #     positive_indices = positive_indices
    # positive_indices = tf.random_shuffle(positive_indices)[:positive_count]
    # positive_count = tf.shape(positive_indices)[0]
    # Negative ROIs. Add enough to maintain positive:negative ratio.
    r = 1.0 / 0.3
    negative_count = config.TRAIN_ROIS_PER_IMAGE  - positive_count
    # negative_count = tf.cast(r * tf.cast(positive_count, tf.float32), tf.int32) - positive_count
    # negative_indices = tf.random_shuffle(negative_indices)[:negative_count]
    #if tf.shape(negative_indices)[0] > negative_count:
    negative_indices = tf.random_shuffle(negative_indices)[:negative_count]
    #else:
    #    negative_indices = negative_indices
    # Gather selected ROIs
    positive_rois = tf.gather(proposals, positive_indices)
    negative_rois = tf.gather(proposals, negative_indices)
    #return positive_rois

    # Assign positive ROIs to GT boxes.
    positive_overlaps = tf.gather(overlaps, positive_indices)
    roi_gt_box_assignment = tf.argmax(positive_overlaps, axis=1)

    # roi_gt_box_assignment = tf.cond(
    #     tf.greater(tf.shape(positive_overlaps)[1], 0),
    #     true_fn = lambda: tf.argmax(positive_overlaps, axis=1),
    #     false_fn = lambda: tf276,.cast(tf.constant([]),tf.int64)
    # )

    gt_center = utils.box_to_center(gt_boxes)
    roi_gt_center = tf.gather(gt_center, roi_gt_box_assignment)
    roi_gt_boxes = tf.gather(gt_boxes, roi_gt_box_assignment)
    roi_gt_class_ids = tf.gather(gt_class_ids, roi_gt_box_assignment)
    # Compute bbox refinement for positive ROIs
    deltas = utils.box_refinement_graph(positive_rois, roi_gt_boxes)
    deltas /= config.BBOX_STD_DEV

    # Assign positive ROIs to GT masks
    # Permute masks to [N, height, width, 1]
    transposed_masks = tf.expand_dims(tf.transpose(gt_masks, [3, 0, 1, 2]), -1)

    # Pick the right mask for each ROI
    roi_masks = tf.gather(transposed_masks, roi_gt_box_assignment)
    #
    # Compute mask targets
    boxes = positive_rois
    box_ids = tf.range(0, tf.shape(roi_masks)[0])
    box_opt = tf.load_op_library('/home/yankun/tensorflow/CR1/crop_and_resize_op_gpu.so')
    masks = box_opt.crop_resize(tf.cast(roi_masks, tf.float32), boxes,
                                     box_ids,
                                     [32, 32, 32])


    # Remove the extra dimension from masks.
    masks = tf.squeeze(masks, axis=4)

    # Threshold mask pixels at 0.5 to have GT masks be 0 or 1 to use with
    # binary cross entropy loss.
    masks = tf.round(masks)

    # Append negative ROIs and pad bbox deltas and masks that
    # are not used for negative ROIs with zeros.
    rois = tf.concat([positive_rois, negative_rois], axis=0)
    N = tf.shape(negative_rois)[0]
    P = tf.maximum(config.TRAIN_ROIS_PER_IMAGE - tf.shape(rois)[0], 0)
    rois = tf.pad(rois, [(0, P), (0, 0)])

    roi_gt_boxes = tf.pad(roi_gt_boxes, [(0, N + P), (0, 0)])

    roi_gt_class_ids = tf.pad(roi_gt_class_ids, [(0, N + P)])
    deltas = tf.pad(deltas, [(0, N + P), (0, 0)])

    roi_gt_center = tf.pad(roi_gt_center,[[0, N + P], (0, 0)])

    masks = tf.pad(masks, [[0, N + P], (0, 0), (0, 0), (0, 0)])
    #print(rois, deltas)
    return rois, roi_gt_class_ids, deltas, masks

class DetectionTargetLayer(KE.Layer):
    """Subsamples proposals and generates target box refinement, class_ids,
    and masks for each.

    Inputs:
    proposals: [batch, N, (y1, x1, y2, x2)] in normalized coordinates. Might
               be zero padded if there are not enough proposals.
    gt_class_ids: [batch, MAX_GT_INSTANCES] Integer class IDs.
    gt_boxes: [batch, MAX_GT_INSTANCES, (y1, x1, y2, x2)] in normalized
              coordinates.
    gt_masks: [batch, height, width, MAX_GT_INSTANCES] of boolean type

    Returns: Target ROIs and corresponding class IDs, bounding box shifts,
    and masks.
    rois: [batch, TRAIN_ROIS_PER_IMAGE, (y1, x1, y2, x2)] in normalized
          coordinates
    target_class_ids: [batch, TRAIN_ROIS_PER_IMAGE]. Integer class IDs.
    target_deltas: [batch, TRAIN_ROIS_PER_IMAGE, NUM_CLASSES,
                    (dy, dx, log(dh), log(dw), class_id)]
                   Class-specific bbox refinements.
    target_mask: [batch, TRAIN_ROIS_PER_IMAGE, height, width)
                 Masks cropped to bbox boundaries and resized to neural
                 network output size.

    Note: Returned arrays might be zero padded if not enough target ROIs.
    """

    def __init__(self, config, **kwargs):
        super(DetectionTargetLayer, self).__init__(**kwargs)
        self.config = config

    def call(self, inputs):
        proposals = inputs[0]
        gt_class_ids = inputs[1]
        gt_boxes = inputs[2]
        gt_masks = inputs[3]

        # Slice the batch and run a graph for each slice
        # TODO: Rename target_bbox to target_deltas for clarity
        names = ["rois", "target_class_ids", "target_bbox", "target_mask", "target_center"]
        outputs = utils.batch_slice(
            [proposals, gt_class_ids, gt_boxes, gt_masks],
            lambda w, x, y, z: detection_targets_graph(
                w, x, y, z, self.config),
            1, names=names)
        #print(outputs)
        return outputs

    def compute_output_shape(self, input_shape):
        return [
            (None, self.config.TRAIN_ROIS_PER_IMAGE, 6),  # rois
            (None, 1),  # class_ids
            (None, self.config.TRAIN_ROIS_PER_IMAGE, 6),  # deltas
            (None, self.config.TRAIN_ROIS_PER_IMAGE, 56, 56, 56)  # masks
        ]

    def compute_mask(self, inputs, mask=None):
        return [None, None, None, None]

##################################################################
# Proposal Layer
#################################################################
class ProposalLayer(KE.Layer):
    """Receives anchor scores and selects a subset to pass as proposals
    to the second stage. Filtering is done based on anchor scores and
    non-max suppression to remove overlaps. It also applies bounding
    box refinement deltas to anchors.

    Inputs:
        rpn_probs: [batch, anchors, (bg prob, fg prob)]
        rpn_bbox: [batch, anchors, (dz, dy, dx, log(dd), log(dh), log(dw)]
        anchors: [batch, (z1, y1, x1, z2, y2, x2)] anchors in normalized coordinates

    Returns:
        Proposals in normalized coordinates [batch, rois, (z1, y1, x1, z2, y2, x2)]
    """

    def __init__(self, proposal_count, nms_threshold, **kwargs):
        super(ProposalLayer, self).__init__(**kwargs)
        self.proposal_count = proposal_count
        self.nms_threshold = nms_threshold

    def call(self, inputs):
        # Box Scores. Use the foreground class confidence. [Batch, num_rois, 1]
        scores = inputs[0][:, :, 1]   #[Batc, num_rois]

        # Box deltas [batch, num_rois, 6]
        deltas = inputs[1]            #[Batch, num_rois, 6]
        deltas = deltas * np.reshape(np.array([0.1, 0.1, 0.1, 0.2, 0.2, 0.2]), [1, 1, 6])
        # Anchors
        anchors = inputs[2]
                                      #[Batch, num_anchors, 6]
        # Improve performance by trimming to top anchors by score
        # and doing the rest on the smaller subset.
        pre_nms_limit = tf.minimum(500, tf.shape(anchors)[1])
        ix = tf.nn.top_k(scores, pre_nms_limit, sorted=True,
                         name="top_anchors").indices

        scores = utils.batch_slice([scores, ix], lambda x, y: tf.gather(x, y),
                                   1)
        deltas = utils.batch_slice([deltas, ix], lambda x, y: tf.gather(x, y),
                                   1)
        pre_nms_anchors = utils.batch_slice([anchors, ix], lambda a, x: tf.gather(a, x),
                                    1,
                                    names=["pre_nms_anchors"])

        # Apply deltas to anchors to get refined anchors.
        # [batch, N, (y1, x1, z1, y2, x2, z2)]
        boxes = utils.batch_slice([pre_nms_anchors, deltas],
                                  lambda x, y: utils.apply_box_deltas_graph(x, y),
                                  1,
                                  names=["refined_anchors"])

        # Clip to image boundaries. Since we're in normalized coordinates,
        # clip to 0..1 range. [batch, N, (y1, x1, y2, x2)]
        window = np.array([0, 0, 0, 1, 1, 1], dtype=np.float32)
        boxes = utils.batch_slice(boxes,
                                  lambda x: utils.clip_boxes_graph(x, window),
                                  1,
                                  names=["refined_anchors_clipped"])

        # Filter out small boxes
        # According to Xinlei Chen's paper, this reduces detection accuracy
        # for small objects, so we're skipping it.

        # Non-max suppression
        def nms(boxes, scores):
            box_opt = tf.load_op_library('/home/yankun/tensorflow/CR/nms.so')

            indices = box_opt.non_max(
                boxes, scores, self.proposal_count,
                self.nms_threshold, name="rpn_non_max_suppression")
            proposals = tf.gather(boxes, indices)
            # Pad if needed
            padding = tf.maximum(self.proposal_count - tf.shape(proposals)[0], 0)
            proposals = tf.pad(proposals, [(0, padding), (0, 0)])
            return proposals
        proposals = utils.batch_slice([boxes, scores], nms,
                                      1)
        return proposals

    def compute_output_shape(self, input_shape):
        return (None, self.proposal_count, 6)

def log2_graph(x):
    """Implementatin of Log2. TF doesn't have a native implemenation."""
    return tf.log(x) / tf.log(2.0)

#################################################################
#     ROI Align Layer
#################################################################
@ops.RegisterGradient("CropResize")
def _crop_resize_grad(op, grad):
            grad = tf.cast(grad, 'float32')
            #print(grad)
            image = op.inputs[0]
            bboxes = op.inputs[1]
            box_idx = op.inputs[2]
            box_opt = tf.load_op_library('/home/yankun/tensorflow/CR1/crop_and_resize_op_gpu.so')

            image_shape = array_ops.shape(image)
            box_shape = array_ops.shape(bboxes)
            grad_image = box_opt.crop_resize_grad_image(grad, bboxes, box_idx, image_shape, tf.float32)
            grad_box = box_opt.crop_resize_grad_boxes(grad, image, bboxes, box_idx)
            return grad_image, grad_box, None, None
class PyramidROIAlign(KE.Layer):
    """Implements ROI Pooling on multiple levels of the feature pyramid.

    Params:
    - pool_shape: [height, width, depth] of the output pooled regions. Usually [7, 7]

    Inputs:
    - boxes: [batch, num_boxes, (z1, y1, x1, z2, y2, x2)] in normalized
             coordinates. Possibly padded with zeros if not enough
             boxes to fill the array.
    - Feature maps: List of feature maps from different levels of the pyramid.
                    Each is [batch, height, width, depth, channels]

    Output:
    Pooled regions in the shape: [batch, num_boxes, height, width, depth, channels].
    The width and height are those specific in the pool_shape in the layer
    constructor.
    """

    def __init__(self, pool_shape, **kwargs):
        super(PyramidROIAlign, self).__init__(**kwargs)
        self.pool_shape = tuple(pool_shape)

    def call(self, inputs):
        # Crop boxes [batch, num_boxes, (y1, x1, y2, x2)] in normalized coords
        boxes = inputs[0]

        # Image meta
        # Holds details about the image. See compose_image_meta()
        feature_maps = inputs[1]
        boxes = tf.cast(boxes, 'float32')

        feature_maps = tf.transpose(feature_maps,(0,2,3,4,1))

        pooled_total = []
        boxes_shape = boxes.get_shape()
        feature_shape = feature_maps.get_shape()
        box_ids = tf.zeros([tf.shape(boxes)[1], ], tf.int32)


        #for j in range(0, boxes_shape[1]):
        box_opt = tf.load_op_library('/home/yankun/tensorflow/CR1/crop_and_resize_op_gpu.so')
        pool_image = box_opt.crop_resize(tf.cast(feature_maps, tf.float32), boxes[0],
                                        box_ids,
                                         self.pool_shape)

        #pooled_total.append(pool_image[0])
        pool_image = tf.transpose(pool_image,(0,4,1,2,3))
        # Re-add the batch dimension
        pooled = tf.expand_dims(pool_image, 0)
        return pooled

    def compute_output_shape(self, input_shape):
        return input_shape[0][:2] + (input_shape[1][1], ) + self.pool_shape

############################################################
#  BackBone Network Head
############################################################

class GroupNormalization(KE.Layer):
    ## Adapted from batnormalization ##

    def __init__(self,
                 groups=32,
                 axis=-1,
                 epsilon=1e-5,
                 center=True,
                 scale=True,
                 beta_initializer='zeros',
                 gamma_initializer='ones',
                 beta_regularizer=None,
                 gamma_regularizer=None,
                 beta_constraint=None,
                 gamma_constraint=None,
                 **kwargs):
        super(GroupNormalization, self).__init__(**kwargs)
        self.supports_masking = True
        self.groups = groups
        self.axis = axis
        self.epsilon = epsilon
        self.center = center
        self.scale = scale
        self.beta_initializer = initializers.get(beta_initializer)
        self.gamma_initializer = initializers.get(gamma_initializer)
        self.beta_regularizer = regularizers.get(beta_regularizer)
        self.gamma_regularizer = regularizers.get(gamma_regularizer)
        self.beta_constraint = constraints.get(beta_constraint)
        self.gamma_constraint = constraints.get(gamma_constraint)

    def build(self, input_shape):
        dim = input_shape[self.axis]

        if dim is None:
            raise ValueError('Axis ' + str(self.axis) + ' of '
                                                        'input tensor should have a defined dimension '
                                                        'but the layer received an input with shape ' +
                             str(input_shape) + '.')

        if dim < self.groups:
            raise ValueError('Number of groups (' + str(self.groups) + ') cannot be '
                                                                       'more than the number of channels (' +
                             str(dim) + ').')

        if dim % self.groups != 0:
            raise ValueError('Number of groups (' + str(self.groups) + ') must be a '
                                                                       'multiple of the number of channels (' +
                             str(dim) + ').')

        self.input_spec = InputSpec(ndim=len(input_shape),
                                    axes={self.axis: dim})
        shape = (dim,)

        if self.scale:
            self.gamma = self.add_weight(shape=shape,
                                         name='gamma',
                                         initializer=self.gamma_initializer,
                                         regularizer=self.gamma_regularizer,
                                         constraint=self.gamma_constraint)
        else:
            self.gamma = None

        if self.center:
            self.beta = self.add_weight(shape=shape,
                                        name='beta',
                                        initializer=self.beta_initializer,
                                        regularizer=self.beta_regularizer,
                                        constraint=self.beta_constraint)
        else:
            self.beta = None
        self.built = True

    def call(self, inputs, **kwargs):
        input_shape = K.int_shape(inputs)
        # Prepare broadcasting shape.
        ndim = len(input_shape)
        reduction_axes = list(range(len(input_shape)))
        del reduction_axes[self.axis]
        broadcast_shape = [1] * len(input_shape)
        broadcast_shape[self.axis] = input_shape[self.axis]

        reshape_group_shape = list(input_shape)
        reshape_group_shape[self.axis] = input_shape[self.axis] // self.groups
        group_shape = [-1, self.groups]
        group_shape.extend(reshape_group_shape[1:])
        group_reduction_axes = list(range(len(group_shape)))

        # Determines whether broadcasting is needed.
        needs_broadcasting = (sorted(reduction_axes) != list(range(ndim))[:-1])
        inputs = K.reshape(inputs, tuple(group_shape))

        mean = K.mean(inputs, axis=group_reduction_axes[2:], keepdims=True)
        variance = K.var(inputs, axis=group_reduction_axes[2:], keepdims=True)

        inputs = (inputs - mean) / (K.sqrt(variance + self.epsilon))

        original_shape = [-1] + list(input_shape[1:])

        inputs = K.reshape(inputs, original_shape)

        if needs_broadcasting:
            outputs = inputs

            # In this case we must explicitly broadcast all parameters.
            if self.scale:
                broadcast_gamma = K.reshape(self.gamma, broadcast_shape)
                outputs = outputs * broadcast_gamma

            if self.center:
                broadcast_beta = K.reshape(self.beta, broadcast_shape)
                outputs = outputs + broadcast_beta
        else:
            outputs = inputs

            if self.scale:
                outputs = outputs * self.gamma

            if self.center:
                outputs = outputs + self.beta
        '''
        outputs = K.reshape(inputs, original_shape) ## changed ##
        '''
        return outputs

    def get_config(self):
        config = {
            'groups': self.groups,
            'axis': self.axis,
            'epsilon': self.epsilon,
            'center': self.center,
            'scale': self.scale,
            'beta_initializer': initializers.serialize(self.beta_initializer),
            'gamma_initializer': initializers.serialize(self.gamma_initializer),
            'beta_regularizer': regularizers.serialize(self.beta_regularizer),
            'gamma_regularizer': regularizers.serialize(self.gamma_regularizer),
            'beta_constraint': constraints.serialize(self.beta_constraint),
            'gamma_constraint': constraints.serialize(self.gamma_constraint)
        }
        base_config = super(GroupNormalization, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return input_shape

def conv_block(input_tensor, kernel_size, filters, stage, block,
               strides=(2, 2), use_bias=True, train_bn=True):
    """conv_block is the block that has a conv layer at shortcut
    # Arguments
        input_tensor: input tensor
        kernel_size: defualt 3, the kernel size of middle conv layer at main path
        filters: list of integers, the nb_filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
        use_bias: Boolean. To use or not use a bias in conv layers.
        train_bn: Boolean. Train or freeze Batch Norm layres
    Note that from stage 3, the first conv layer at main path is with subsample=(2,2)
    And the shortcut should have subsample=(2,2) as well
    """
    nb_filter1, nb_filter2, nb_filter3 = filters
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = KL.Conv3D(nb_filter2, (kernel_size, kernel_size, kernel_size), padding='same',
                  data_format='channels_first', kernel_initializer='he_normal', name=conv_name_base + '1a')(input_tensor)
    x = GroupNormalization(groups=16, axis=1, name=bn_name_base + '1a')(x)
    x = KL.Activation('relu')(x)

    x = KL.Conv3D(nb_filter3, (kernel_size, kernel_size, kernel_size), padding='same',
                  data_format='channels_first', kernel_initializer='he_normal', name=conv_name_base + '1b')(x)
    x = GroupNormalization(groups=16, axis=1, name=bn_name_base + '1b')(x)

    if nb_filter1 == nb_filter3:
        shortcut = input_tensor
        x = Add()([shortcut, x])

    else:
        y = Conv3D(nb_filter3, (1, 1, 1), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal',name=conv_name_base + '1c')(input_tensor)
        x = GroupNormalization(groups=16, axis=1, name=bn_name_base + '1c')(x)

        x = Add()([y, x])
    x = KL.Activation('relu', name='res' + str(stage) + block + '_out')(x)
    return x

class res_block:
    def __init__(self, in_channels, inter_channels, out_channels, **kwargs):
        self.in_channels = in_channels
        self.inter_channels = inter_channels
        self.out_channels = out_channels

    def __call__(self, input_):

        x = Conv3D(self.inter_channels, (3, 3, 3), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(input_)

        x = GroupNormalization(groups=16, axis=1, name = 'gn_1')(x)

        x = Activation('relu')(x)

        x = Conv3D(self.out_channels, (3, 3, 3), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(x)

        x = GroupNormalization(groups=16, axis=1)(x)
        if self.in_channels == self.out_channels:
            shortcut = input_
            x = Add()([shortcut, x])

        else:
            y = Conv3D(self.out_channels, (1, 1, 1), padding='same', data_format='channels_first',
                       kernel_initializer='he_normal')(input_)
            x = GroupNormalization(groups=16, axis=1)(x)

            x = Add()([y, x])
        x = Activation('relu')(x)
        return x

def resnet_graph(input_image):
    """Build a ResNet graph.
        architecture: Can be resnet50 or resnet101
        stage5: Boolean. If False, stage5 of the network is not created
        train_bn: Boolean. Train or freeze Batch Norm layres
    """
    conv1 = conv_block(input_image, 3, [1, 32, 32], stage=1, block='a')
    #conv1 = res_block(1, 32, 32, name = 'conv1')(input_image)
    pool1 = MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2), data_format='channels_first')(conv1)

    conv2 = conv_block(pool1, 3, [16, 64, 64], stage=2, block='a')
    #conv2 = res_block(32, 32, 32)(pool1)
    pool2 = MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2), data_format='channels_first')(conv2)

    conv3 = conv_block(pool2, 3, [64, 64, 128], stage=3, block='a')
    #conv3 = res_block(32, 32, 32)(pool2)
    pool3 = MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2), data_format='channels_first')(conv3)

    conv4 = conv_block(pool3, 3, [128, 128, 128], stage=4, block='a')
    #conv4 = res_block(32, 32, 32)(pool3)
    pool4 = MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2), data_format='channels_first')(conv4)

    conv5 = conv_block(pool4, 3, [128, 128, 128], stage=5, block='a')
    # #conv5 = res_block(32, 32, 32)(pool4)
    # pool5 = MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2), data_format='channels_first')(conv5)
    return conv1, conv2, conv3, conv4, conv5

class deconv_block:
    def __init__(self, inter_channels, out_channels, **kwargs):
        self.inter_channels = inter_channels
        self.out_channels = out_channels

    def __call__(self, input_, input_2):
        up_input_ = UpSampling3D(size=(2, 2, 2), data_format='channels_first')(input_)
        x = Concatenate(axis=1)([up_input_, input_2])
        x = Conv3D(self.inter_channels, (3, 3, 3), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(x)
        x = GroupNormalization(groups=16, axis=1)(x)
        x = Activation('relu')(x)
        x = Conv3D(self.out_channels, (3, 3, 3), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(x)
        x = GroupNormalization(groups=16, axis=1)(x)
        x = Activation('relu')(x)
        return x
class deconv_block_non:
    def __init__(self, inter_channels, out_channels, **kwargs):
        self.inter_channels = inter_channels
        self.out_channels = out_channels

    def __call__(self, input_, input_2):
        up_input_ = UpSampling3D(size=(2, 2, 2), data_format='channels_first')(input_)
        #x = Concatenate(axis=1)([up_input_, input_2])
        x = Conv3D(self.inter_channels, (3, 3, 3), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(up_input_)
        x = GroupNormalization(groups=16, axis=1)(x)
        x = Activation('relu')(x)
        x = Conv3D(self.out_channels, (3, 3, 3), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(x)
        x = GroupNormalization(groups=16, axis=1)(x)
        x = Activation('relu')(x)
        return x
class deconv_block1:
    def __init__(self, in_channels, inter_channels, out_channels, **kwargs):
        self.in_channels = in_channels
        self.inter_channels = inter_channels
        self.out_channels = out_channels

    def __call__(self, input_, input_2):
        up_input_ = UpSampling3D(size=(2, 2, 2), data_format='channels_first')(input_)
        x = Concatenate(axis=1)([up_input_, input_2])
        conca = x
        x = Conv3D(self.inter_channels, (3, 3, 3), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(x)
        x = GroupNormalization(groups=16, axis=1)(x)
        x = Activation('relu')(x)
        x = Conv3D(self.out_channels, (3, 3, 3), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(x)
        x = GroupNormalization(groups=16, axis=1)(x)
        if self.in_channels == self.out_channels:
            shortcut = conca
            x = Add()([shortcut, x])

        else:
            y = Conv3D(self.out_channels, (1, 1, 1), padding='same', data_format='channels_first',
                   kernel_initializer='he_normal')(conca)
            y = GroupNormalization(groups=16, axis=1)(y)
            shortcut = y
            x = Add()([shortcut, x])

        x = Activation('relu')(x)
        return x
def expand_graph(conv1, conv2, conv3, conv4,conv5):
    """Build a ResNet graph.
        architecture: Can be resnet50 or resnet101
        stage5: Boolean. If False, stage5 of the network is not created
        train_bn: Boolean. Train or freeze Batch Norm layres
    """
    deconv1 = deconv_block(128, 128)(conv5, conv4)
    deconv2 = deconv_block(128, 128)(deconv1, conv3)
    deconv3 = deconv_block(64, 64)(deconv2, conv2)
    deconv4 = deconv_block(32, 32)(deconv3, conv1)

    # #conv5 = res_block(32, 32, 32)(pool4)
    # pool5 = MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2), data_format='channels_first')(conv5)
    return deconv4


############################################################
#  Loss Functions
############################################################

def smooth_l1_loss(y_true, y_pred):
    """Implements Smooth-L1 loss.
    y_true and y_pred are typicallly: [N, 4], but could be any shape.
    """
    diff = K.abs(y_true - y_pred)
    less_than_one = K.cast(K.less(diff, 1.0), "float32")
    loss = (less_than_one * 0.5 * diff**2) + (1 - less_than_one) * (diff - 0.5)
    return loss


def rpn_class_loss_graph(rpn_match, rpn_class_logits):
    """RPN anchor classifier loss.

    rpn_match: [batch, anchors, 1]. Anchor match type. 1=positive,
               -1=negative, 0=neutral anchor.
    rpn_class_logits: [batch, anchors, 2]. RPN classifier logits for FG/BG.
    """
    # Squeeze last dim to simplify
    rpn_match = tf.cast(rpn_match, 'int64')
    rpn_match = K.reshape(rpn_match, (-1,))
    rpn_class_logits =  K.reshape(rpn_class_logits, (-1,2))
    #rpn_match = tf.squeeze(rpn_match, -1)
    # Get anchor classes. Convert the -1/+1 match to 0/1 values.
    #anchor_class = K.cast(K.equal(rpn_match, 1), tf.int32)
    # Positive and Negative anchors contribute to the loss,
    # but neutral anchors (match value = 0) don't.
    indices = tf.where(K.not_equal(rpn_match, -1))[:, 0]

    # Pick rows that contribute to the loss and filter out the rest.
    rpn_class_logits = tf.gather(rpn_class_logits, indices)
    anchor_class = tf.gather(rpn_match, indices)
    # Crossentropy loss
    loss = K.sparse_categorical_crossentropy(target=anchor_class,
                                             output=rpn_class_logits,
                                             )

    loss = K.switch(tf.size(loss) > 0, K.mean(loss), tf.constant(0.0))
    return loss


def rpn_bbox_loss_graph(config, target_bbox, rpn_match, rpn_bbox):
    """Return the RPN bounding box loss graph.

    config: the model config object.
    target_bbox: [batch, max positive anchors, (dy, dx, log(dh), log(dw))].
        Uses 0 padding to fill in unsed bbox deltas.
    rpn_match: [batch, anchors, 1]. Anchor match type. 1=positive,
               -1=negative, 0=neutral anchor.
    rpn_bbox: [batch, anchors, (dy, dx, log(dh), log(dw))]
    """
    # Positive anchors contribute to the loss, but negative and
    # neutral anchors (match value of 0 or -1) don't.
    rpn_match = K.squeeze(rpn_match, -1)
    indices = tf.where(K.not_equal(rpn_match, -1))

    # Pick bbox deltas that contribute to the loss
    rpn_bbox = tf.gather_nd(rpn_bbox, indices)

    # Trim target bounding box deltas to the same length as rpn_bbox.
    batch_counts = K.sum(K.cast(K.not_equal(rpn_match, -1), tf.int32), axis=1)

    def batch_pack_graph(x, counts, num_rows):
        """Picks different number of values from each row
        in x depending on the values in counts.
        """
        outputs = []
        for i in range(num_rows):
            outputs.append(x[i, :counts[i]])
        return tf.concat(outputs, axis=0)
    target_bbox = batch_pack_graph(target_bbox, batch_counts,
                                   config.IMAGES_PER_GPU)

    # TODO: use smooth_l1_loss() rather than reimplementing here
    #       to reduce code duplication
    diff = K.abs(target_bbox - rpn_bbox)
    less_than_one = K.cast(K.less(diff, 1.0), "float32")
    loss = (less_than_one * 0.5 * diff**2) + (1 - less_than_one) * (diff - 0.5)

    loss = K.switch(tf.size(loss) > 0, K.mean(loss), tf.constant(0.0))
    return loss
def rpn_bbox_loss_graph1(target_bbox, rpn_match, rpn_bbox):
        """Return the RPN bounding box loss graph.
        config: the model config object.
        target_bbox: [batch, max positive anchors, (dy, dx, log(dh), log(dw))].
            Uses 0 padding to fill in unsed bbox deltas.
        rpn_match: [batch, anchors, 1]. Anchor match type. 1=positive,
                   -1=negative, 0=neutral anchor.
        rpn_bbox: [batch, anchors, (dy, dx, log(dh), log(dw))]
        """
        # Positive anchors contribute to the loss, but negative and
        # neutral anchors (match value of 0 or -1) don't.
        rpn_match = K.reshape(rpn_match, (-1,))
        target_bbox = K.reshape(target_bbox, (-1, 3))  # [26,3]

        rpn_bbox = K.reshape(rpn_bbox, (-1, 3))  # [26, 3]
        #print(rpn_match)
        indices = tf.where(K.equal(rpn_match, 1))[:, 0]
        print(indices)
        # Pick bbox deltas that contribute to the loss
        rpn_bbox = tf.gather(rpn_bbox, indices)
        target_bbox = tf.gather(target_bbox, indices)
        print(rpn_bbox,target_bbox)
        # Trim target bounding box deltas to the same length as rpn_bbox.
        # batch_counts = K.sum(K.cast(K.equal(rpn_match, 1), tf.int32), axis=1)
        # print(sess.run(batch_counts))
        # target_bbox = batch_pack_graph(target_bbox, batch_counts, 1)

        # TODO: use smooth_l1_loss() rather than reimplementing here
        #       to reduce code duplication
        diff = K.abs(target_bbox - rpn_bbox)
        less_than_one = K.cast(K.less(diff, 1.0), "float32")
        loss = (less_than_one * 0.5 * diff ** 2) + (1 - less_than_one) * (diff - 0.5)

        loss = K.switch(tf.size(loss) > 0, K.mean(loss), tf.constant(0.0))
        return loss


def rpn_bbox_loss_graph2(target_heatmap, pred_heatmap):
    """Return the RPN bounding box loss graph.
    config: the model config object.
    target_bbox: [batch, num_of_lmk, (Z,Y,X)].
        Uses 0 padding to fill in unsed bbox deltas.
    pred_heatmap: [batch, num_of_lmk, (Z,Y,X)]. Anchor match type. 1=positive,
               -1=negative, 0=neutral anchor.
    rpn_bbox: [batch, anchors, (dy, dx, log(dh), log(dw))]
    """
    # Positive anchors contribute to the loss, but negative and
    # neutral anchors (match value of 0 or -1) don't.
    # loss1 = K.switch(tf.size(target_heatmap) > 0,
    #                  smooth_l1_loss(y_true=target_heatmap, y_pred=pred_heatmap),
    #                  tf.constant(0.0))
    # loss1 = K.mean(loss1)
    loss1 = K.mean(K.square(target_heatmap - pred_heatmap))
    return loss1

def lmk_regression_loss_graph2(target_heatmap, pred_heatmap):
    """Return the RPN bounding box loss graph.
    config: the model config object.
    target_bbox: [batch, num_of_lmk, (Z,Y,X)].
        Uses 0 padding to fill in unsed bbox deltas.
    pred_heatmap: [batch, num_of_lmk, (Z,Y,X)]. Anchor match type. 1=positive,
               -1=negative, 0=neutral anchor.
    rpn_bbox: [batch, anchors, (dy, dx, log(dh), log(dw))]
    """
    # Positive anchors contribute to the loss, but negative and
    # neutral anchors (match value of 0 or -1) don't.
    # loss1 = K.switch(tf.size(target_heatmap) > 0,
    #                  smooth_l1_loss(y_true=target_heatmap, y_pred=pred_heatmap),
    #                  tf.constant(0.0))
    # loss1 = K.mean(loss1)
    print(target_heatmap,pred_heatmap)
    loss1 = K.mean(K.square(target_heatmap - pred_heatmap))
    return loss1
def class_loss_graph(rpn_match, class_logits):
    """RPN anchor classifier loss.

    rpn_match: [batch, 26, 1]. Anchor match type. 1=positive,
               -1=negative, 0=neutral anchor.
    rpn_class_logits: [batch, 26, 2]. RPN classifier logits for FG/BG.
    """
    # Squeeze last dim to simplify
    print(rpn_match, class_logits)
    rpn_match = K.reshape(rpn_match, (-1,1))
    class_logits = K.reshape(class_logits, (-1,2))

    # Get anchor classes. Convert the -1/+1 match to 0/1 values.

    #class_logits = tf.squeeze(class_logits, -1)
    # Positive and Negative anchors contribute to the loss,
    # but neutral anchors (match value = 0) don't.
    # indices = tf.where(K.not_equal(rpn_match, 0))
    #
    # # Pick rows that contribute to the loss and filter out the rest.
    # rpn_class_logits = tf.gather_nd(rpn_class_logits, indices)
    # anchor_class = tf.gather_nd(anchor_class, indices)
    # # Crossentropy loss
    loss = K.sparse_categorical_crossentropy(target=rpn_match,
                                             output=class_logits,
                                             )
    loss = K.switch(tf.size(loss) > 0, K.mean(loss), tf.constant(0.0))
    return loss
def mrcnn_class_loss_graph(target_class_ids, pred_class_logits,
                           active_class_ids):
    """Loss for the classifier head of Mask RCNN.

    target_class_ids: [batch, num_rois]. Integer class IDs. Uses zero
        padding to fill in the array.
    pred_class_logits: [batch, num_rois, num_classes]
    active_class_ids: [batch, num_classes]. Has a value of 1 for
        classes that are in the dataset of the image, and 0
        for classes that are not in the dataset.
    """
    # During model building, Keras calls this function with
    # target_class_ids of type float32. Unclear why. Cast it
    # to int to get around it.
    target_class_ids = tf.cast(target_class_ids, 'int64')
    K.print_tensor(target_class_ids, message='y_true = ')
    # Find predictions of classes that are not in the dataset.
    pred_class_ids = tf.argmax(pred_class_logits, axis=2)
    # TODO: Update this line to work with batch > 1. Right now it assumes all
    #       images in a batch have the same active_class_ids
    pred_active = tf.gather(active_class_ids[0], pred_class_ids)

    # Loss
    loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
        labels=target_class_ids, logits=pred_class_logits)

    # Erase losses of predictions of classes that are not in the active
    # classes of the image.
    pred_active = tf.cast(pred_active, 'float32')
    loss = loss * pred_active

    # Computer loss mean. Use only predictions that contribute
    # to the loss to get a correct mean.
    loss = tf.reduce_sum(loss) / tf.reduce_sum(pred_active)
    return loss
def mrcnn_bbox_loss_graph1(target_lmk, target_class_ids, pred_lmk):
    """Loss for Mask R-CNN bounding box refinement.
    rois: [batch, num_rois, (z1, x1, y1, z2, x2, y2)] in normalized coordinates
    target_bbox: [batch, 26, 3]
    target_class_ids: [batch, 26]. Integer class IDs.
    pred_bbox: [batch, 26, 3]
    """
    # Reshape to merge batch and roi dimensions for simplicity.

    target_class_ids = K.reshape(target_class_ids, (-1,))      # [26,]
    target_bbox = K.reshape(target_lmk, (-1, 3))              # [26,3]

    pred_bbox = K.reshape(pred_lmk, (-1, 3)) #[26, 3]

    # Only positive ROIs contribute to the loss. And only
    # the right class_id of each ROI. Get their indicies.
    positive_roi_ix = tf.where(target_class_ids > 0)[:, 0]
    positive_roi_ix = tf.cast(positive_roi_ix, 'int32')

    # Gather the deltas (predicted and true) that contribute to loss
    target_bbox = tf.gather(target_bbox, positive_roi_ix)
    pred_bbox = tf.gather_nd(pred_bbox, positive_roi_ix)
    #Smooth-L1 Loss
    loss1 = K.switch(tf.size(target_bbox) > 0,
                    smooth_l1_loss(y_true=target_bbox, y_pred=pred_bbox),
                    tf.constant(0.0))
    loss1 = K.mean(loss1)


    return loss1

def mrcnn_bbox_loss_graph(target_bbox, target_class_ids, pred_bbox, rois):
    """Loss for Mask R-CNN bounding box refinement.
    rois: [batch, num_rois, (z1, x1, y1, z2, x2, y2)] in normalized coordinates
    target_bbox: [batch, num_rois, (dy, dx, log(dh), log(dw))]
    target_class_ids: [batch, num_rois]. Integer class IDs.
    pred_bbox: [batch, num_rois, num_classes, (dy, dx, log(dh), log(dw))]
    """
    # Reshape to merge batch and roi dimensions for simplicity.
    BBOX_STD_DEV = np.array([0.1, 0.1,0.1, 0.2, 0.2, 0.2])
    BBOX_STD_DEV = tf.cast(tf.constant(BBOX_STD_DEV), 'float32')
    target_class_ids = K.reshape(target_class_ids, (-1,))      # [100,]
    target_bbox = K.reshape(target_bbox, (-1, 6))              # [100,6]
    rois = K.reshape(rois, (-1, 6))
    pred_bbox = K.reshape(pred_bbox, (-1, K.int_shape(pred_bbox)[2], 6)) #[100, num_class, 6]

    # Only positive ROIs contribute to the loss. And only
    # the right class_id of each ROI. Get their indicies.
    positive_roi_ix = tf.where(target_class_ids > 0)[:, 0]
    positive_roi_ix = tf.cast(positive_roi_ix, 'int32')
    positive_roi_class_ids = tf.cast(
        tf.gather(target_class_ids, positive_roi_ix), tf.int32)
    indices = tf.stack([positive_roi_ix, positive_roi_class_ids], axis=1)
    positive_num = tf.shape(positive_roi_ix)[0]
    positive_num = tf.cast(positive_num, 'float')
    # Gather the deltas (predicted and true) that contribute to loss
    target_bbox = tf.gather(target_bbox, positive_roi_ix)
    pred_bbox = tf.gather_nd(pred_bbox, indices)
    #Smooth-L1 Loss
    loss1 = K.switch(tf.size(target_bbox) > 0,
                    smooth_l1_loss(y_true=target_bbox, y_pred=pred_bbox),
                    tf.constant(0.0))
    loss1 = K.mean(loss1)

    rois = tf.gather(rois, positive_roi_ix)
    refined_pred_bbox = utils.apply_box_deltas_graph(rois, pred_bbox * BBOX_STD_DEV)
    traget_refined = utils.apply_box_deltas_graph(rois, target_bbox * BBOX_STD_DEV)
    refined_center = utils.box_to_center(refined_pred_bbox)
    target_center = utils.box_to_center(traget_refined)

    loss2 = tf.keras.losses.mean_squared_error(y_true=target_center, y_pred=refined_center)
    return loss2



def mrcnn_mask_loss_graph(target_masks, target_class_ids, pred_masks):
    """Mask binary cross-entropy loss for the masks head.

    target_masks: [batch, num_rois, depth, height, width].
        A float32 tensor of values 0 or 1. Uses zero padding to fill array.
    target_class_ids: [batch, num_rois]. Integer class IDs. Zero padded.
    pred_masks: [batch, proposals, depth, height, width, num_classes] float32 tensor
                with values from 0 to 1.
    """
    # Reshape for simplicity. Merge first two dimensions into one.


    target_class_ids = K.reshape(target_class_ids, (-1,))
    mask_shape = tf.shape(target_masks)
    target_masks = K.reshape(target_masks, (-1, mask_shape[2], mask_shape[3], mask_shape[4]))
    pred_shape = tf.shape(pred_masks)
    pred_masks = K.reshape(pred_masks,
                           (-1, pred_shape[2], pred_shape[3], pred_shape[4], pred_shape[5]))

    # Permute predicted masks to [N, num_classes, height, width]
    # pred_masks = tf.transpose(pred_masks, [0, 4, 1, 2, 3])
    # Only positive ROIs contribute to the loss. And only
    # the class specific mask of each ROI.
    positive_ix = tf.where(target_class_ids > 0)[:, 0]

    positive_ix = tf.cast(positive_ix, 'int32')

    positive_class_ids = tf.cast(
        tf.gather(target_class_ids, positive_ix), tf.int32)
    indices = tf.stack([positive_ix, positive_class_ids], axis=1)
    #positive_ix_1 = tf.reduce_sum(tf.one_hot(positive_ix_1, tf.shape(target_class_ids)[0], dtype='int32'), axis=1)
    # Gather the masks (predicted and true) that contribute to loss
    y_true = tf.gather(target_masks, positive_ix)
    y_pred = tf.gather_nd(pred_masks, indices)
    #pred_bbox = tf.cast(y_pred, 'int32')

    # Compute binary cross entropy. If no positive ROIs, then return 0.
    # shape: [batch, roi, num_classes]
    loss = K.switch(tf.size(y_true) > 0,
                    K.binary_crossentropy(target=y_true, output=y_pred),
                    tf.constant(0.0))
    loss = K.mean(loss)
    return loss
def keypoint_mrcnn_mask_loss_graph(target_keypoints, target_class_ids, pred_keypoints_logit, mask_shape=[32,32,32],number_point=1):
    """Mask softmax cross-entropy loss for the keypoint head.
    target_keypoints: [batch, num_rois, num_keypoints].
        A int32 tensor of values between[0, 56*56). Uses zero padding to fill array.
    keypoint_weight:[num_person, num_keypoint]
        0: not visible for the coressponding roi
        1: visible for the coressponding roi
    target_class_ids: [batch, num_rois]. Integer class IDs. Zero padded.
    pred_keypoints_logit: [batch, proposals, num_keypoints, height*width] float32 tensor
                with values from 0 to 1.
    """
    # Reshape for simplicity. Merge first two dimensions into one.
    #shape:[N]
    #print(pred_keypoints_logit)
    target_class_ids = K.reshape(target_class_ids, (-1,))
    # Only positive person ROIs contribute to the loss. And only
    # the people specific mask of each ROI.
    positive_people_ix = tf.where(target_class_ids > 0)[:, 0]
    positive_people_ids = tf.cast(
        tf.gather(target_class_ids, positive_people_ix), tf.int64)

    ###Step 1 Get the positive target and predict keypoint masks
        # reshape target_keypoint_weights to [N, num_keypoints]
        # reshape target_keypoint_masks to [N, 17]
    target_keypoints = K.reshape(target_keypoints, (
        -1,  number_point))

    # reshape pred_keypoint_masks to [N, 17, 56*56]
    pred_keypoints_logit = K.reshape(pred_keypoints_logit,
                                    (-1, number_point, mask_shape[0]*mask_shape[1]*mask_shape[2]))

        # Gather the keypoint masks (target and predict) that contribute to loss
        # shape: [N_positive, 17]
    positive_target_keypoints = tf.cast(tf.gather(target_keypoints, positive_people_ix),tf.int32)
    # shape: [N_positive,17, 56*56]
    positive_pred_keypoints_logit = tf.gather(pred_keypoints_logit, positive_people_ix)
        # positive target_keypoint_weights to[N_positive, 17]

    loss = K.switch(tf.size(positive_target_keypoints) > 0,
                    lambda: tf.nn.sparse_softmax_cross_entropy_with_logits(logits=positive_pred_keypoints_logit, labels=positive_target_keypoints),
                    lambda: tf.constant(0.0))

    loss = K.mean(loss)

    return loss
############################################################
#  Fast RCNN and Mask
############################################################
def ROIAlign(rois, feature_maps,
                         pool_size, num_classes, train_bn=True,
                         fc_layers_size=256):
    share = PyramidROIAlign([pool_size, pool_size, pool_size])([rois, feature_maps])
    return share
def fpn_classifier_graph(share,
                         pool_size, num_classes, train_bn=True,
                         fc_layers_size=64):
    """Builds the computation graph of the feature pyramid network classifier
    and regressor heads.

    rois: [batch, num_rois, (y1, x1, y2, x2)] Proposal boxes in normalized
          coordinates.
    feature_maps: List of feature maps from diffent layers of the pyramid,
                  [P2, P3, P4, P5]. Each has a different resolution.
    - image_meta: [batch, (meta data)] Image details. See compose_image_meta()
    pool_size: The width of the square feature map generated from ROI Pooling.
    num_classes: number of classes, which determines the depth of the results
    train_bn: Boolean. Train or freeze Batch Norm layres
    fc_layers_size: Size of the 2 FC layers

    Returns:
        logits: [N, NUM_CLASSES] classifier logits (before softmax)
        probs: [N, NUM_CLASSES] classifier probabilities
        bbox_deltas: [N, (dy, dx, log(dh), log(dw))] Deltas to apply to
                     proposal boxes
    """
    # ROI Pooling
    # Shape: [batch, num_boxes, pool_height, pool_width, channels]

    #share = PyramidROIAlign([pool_size, pool_size, pool_size])([rois, feature_maps])
    # Two 1024 FC layers (implemented with Conv2D for consistency)
    #
    x = KL.TimeDistributed(KL.Conv3D(fc_layers_size, (pool_size, pool_size, pool_size), data_format='channels_first', padding="valid"),
                           name="mrcnn_class_conv1")(share)
    x = KL.TimeDistributed(GroupNormalization(groups=16, axis=1),
                           name='mrcnn_class_bn')(x)
    x = KL.Activation('relu')(x)
    x = KL.TimeDistributed(KL.Conv3D(fc_layers_size, (1, 1, 1), data_format='channels_first'), name="mrcnn_class_conv2")(x)
    x = KL.TimeDistributed(GroupNormalization(groups=16, axis=1),
                           name='mrcnn_class_bn1')(x)
    x = KL.Activation('relu')(x)

    shared = KL.Lambda(lambda x: K.squeeze(K.squeeze(K.squeeze(x, 5), 4),3),
                                   name='shared')(x)

    # Classifier head
    mrcnn_class_logits = KL.TimeDistributed(KL.Dense(num_classes),
                                            name='mrcnn_class_logits')(shared)
    mrcnn_probs = KL.TimeDistributed(KL.Activation("softmax"),
                                     name="mrcnn_class")(mrcnn_class_logits)

    # BBox head
    # [batch, boxes, num_classes * (dy, dx, log(dh), log(dw))]
    x = KL.TimeDistributed(KL.Dense(num_classes * 6, activation='linear'),
                           name='mrcnn_bbox_fc')(shared)

    s = K.int_shape(x)
    mrcnn_bbox = KL.Reshape((s[1], num_classes, 6), name="mrcnn_bbox")(x)

    # # Center head
    # x = KL.TimeDistributed(KL.Dense(num_classes * 3, activation='linear'),
    #                        name='mrcnn_center_fc')(shared)
    # s = K.int_shape(x)
    # mrcnn_center = KL.Reshape((s[1], num_classes, 3), name="mrcnn_center")(x)

    return mrcnn_class_logits, mrcnn_probs, mrcnn_bbox


def build_fpn_mask_graph(share,
                         pool_size, num_classes, train_bn=True):
    """Builds the computation graph of the mask head of Feature Pyramid Network.

    rois: [batch, num_rois, (y1, x1, y2, x2)] Proposal boxes in normalized
          coordinates.
    feature_maps: List of feature maps from diffent layers of the pyramid,
                  [P2, P3, P4, P5]. Each has a different resolution.
    image_meta: [batch, (meta data)] Image details. See compose_image_meta()
    pool_size: The width of the square feature map generated from ROI Pooling.
    num_classes: number of classes, which determines the depth of the results
    train_bn: Boolean. Train or freeze Batch Norm layres

    Returns: Masks [batch, roi_count, height, width, num_classes]
    """
    # ROI Pooling
    # Shape: [batch, boxes, pool_height, pool_width, channels]
    #share = PyramidROIAlign([pool_size, pool_size, pool_size], name="roi_align_classifier")([rois, feature_maps])

    # Conv layers
    x = KL.TimeDistributed(KL.Conv3D(32, (3, 3, 3), data_format='channels_first', padding="same"),
                           name="mrcnn_mask_conv1")(share)
    x = KL.TimeDistributed(GroupNormalization(groups=16, axis=1),
                           name='mrcnn_mask_bn_1')(x)
    x = KL.Activation('relu')(x)

    x = KL.TimeDistributed(KL.Conv3D(32, (3, 3, 3), data_format='channels_first', padding="same"),
                           name="mrcnn_mask_conv2")(x)
    x = KL.TimeDistributed(GroupNormalization(groups=16, axis=1),
                           name='mrcnn_mask_bn_2')(x)
    x = KL.Activation('relu')(x)

    x = KL.TimeDistributed(KL.Conv3D(32, (3, 3, 3), data_format='channels_first', padding="same"),
                           name="mrcnn_mask_conv3")(x)
    x = KL.TimeDistributed(GroupNormalization(groups=16, axis=1),
                           name='mrcnn_mask_bn_3')(x)
    x = KL.Activation('relu')(x)

    x = KL.TimeDistributed(KL.Conv3D(32, (3, 3, 3), data_format='channels_first', padding="same"),
                           name="mrcnn_mask_conv4")(x)
    x = KL.TimeDistributed(GroupNormalization(groups=16, axis=1),
                           name='mrcnn_mask_bn_4')(x)
    x = KL.Activation('relu')(x)
    #
    x = KL.TimeDistributed(KL.Conv3DTranspose(32, (2, 2, 2), data_format='channels_first', strides=4, activation="relu"),
                           name="mrcnn_mask_deconv")(x)
    x = KL.TimeDistributed(KL.Conv3D(num_classes, (1, 1, 1), data_format='channels_first', strides=1, activation="sigmoid"),
                           name="mrcnn_mask")(x)
    return x

def build_fpn_keypoint_graph(share,
                         image_shape, pool_size, num_keypoints):
    """Builds the computation graph of the keypoint head of Feature Pyramid Network.
    rois: [batch, num_rois, (y1, x1, y2, x2)] Proposal boxes in normalized
          coordinates.
    feature_maps: List of feature maps from diffent layers of the pyramid,
                  [P2, P3, P4, P5]. Each has a different resolution.
    image_shape: [height, width, depth]
    pool_size: The width of the square feature map generated from ROI Pooling.
    num_keypoints: number of keypoints, which determines the depth of the results,
    in coco, it's 17, in AI challenger, it's 14
    Returns: keypoint_masks [batch, roi_count, num_keypoints, height*width]
    """

    # ROI Pooling
    # Shape: [batch, num_rois, pool_height, pool_width, channels]
    #print(share)
    for i in range(5):
        x = KL.TimeDistributed(KL.Conv3D(32, (3, 3, 3), data_format='channels_first', padding="same"),
                               name="mrcnn_keypoint_conv_{}".format(i))(share)

        x = KL.TimeDistributed(GroupNormalization(groups=16, axis=1),
                               name='mrcnn_keypoint_mask_bn{}'.format(i))(x)
        x = KL.Activation('relu')(x)

    x = KL.TimeDistributed(KL.Conv3DTranspose(32, (2, 2, 2), data_format='channels_first', strides=4, activation="relu"),
                           name="mrcnn_keypoint_mask_deconv")(x)
    print(x)
    x = KL.TimeDistributed(GroupNormalization(groups=16, axis=1),
                           name='mrcnn_keypoint_mask_bn')(x)    # shape: batch_size, num_roi, num_keypoint, 56, 56
    x = KL.TimeDistributed(KL.Lambda(lambda x: tf.transpose(x,[0,4,1,2,3])), name="mrcnn_keypoint_mask_transpose")(x)
    s = K.int_shape(x)
    x = KL.Reshape((s[1], num_keypoints, -1), name='mrcnn_keypoint_mask_reshape')(x)
    return x
def train(augmentation=None):
    LOSS_WEIGHTS = {
        "rpn_class_loss": 1.,
        "rpn_bbox_loss": 1.,
        "mrcnn_class_loss": 1.,
        "mrcnn_bbox_loss": 1.,
        "mrcnn_mask_loss": 1.
    }
    train_list = np.arange(1, 20)
    validation_list = np.array([21])
    learning_rate = 0.00001
    epoch = 100

    train_generator = utils.data_generator(train_list, Config, shuffle=True,
                                           augmentation=augmentation,
                                           batch_size=1)
    val_generator = utils.data_generator(validation_list, Config, shuffle=True,
                                         batch_size=1)

    # Callbacks
    callbacks = [ModelCheckpoint('model.best.hd5', verbose=0, save_weights_only=True)]  #

    IMAGE_SHAPE = [160, 144, 144]

    # Inputs
    input_image = KL.Input(
        shape=[1, None, None, None], name="input_image", dtype=tf.float32)
    input_rpn_match = KL.Input(
        shape=[None, 1], name="input_rpn_match", dtype=tf.int32)
    input_rpn_bbox = KL.Input(
        shape=[None, 6], name="input_rpn_bbox", dtype=tf.float32)

    # Detection GT (class IDs, bounding boxes, and masks)
    # 1. GT Class IDs (zero padded)
    input_gt_class_ids = KL.Input(
        shape=[None], name="input_gt_class_ids", dtype=tf.int32)
    # 2. GT Boxes in pixels (zero padded)
    # [batch, MAX_GT_INSTANCES, (z1, y1, x1, z2, y2, x2)] in image coordinates
    input_gt_boxes = KL.Input(
        shape=[None, 6], name="input_gt_boxes", dtype=tf.float32)
    # Normalize coordinates
    gt_boxes = KL.Lambda(lambda x: utils.norm_boxes_graph(
        x, K.shape(input_image)[2:5]))(input_gt_boxes)
    # 3. GT Masks (zero padded)
    # [batch, depth, height, width, MAX_GT_INSTANCES]
    input_anchors = KL.Input(shape=[None, 6], name="input_anchors")
    active_class_ids = Input(shape=[1, ], name='active_class_ids', dtype=tf.int32)
    input_gt_masks = KL.Input(
        shape=[IMAGE_SHAPE[0], IMAGE_SHAPE[1], IMAGE_SHAPE[2], None],
        name="input_gt_masks", dtype=bool)
    C5 = resnet_graph(input_image)
    rpn_feature_maps = C5
    mrcnn_feature_maps = C5

    # Anchors
    anchors = input_anchors

    # RPN Model
    rpn = build_rpn_model(Config.RPN_ANCHOR_STRIDE,
                                 len(Config.RPN_ANCHOR_RATIOS))
    # Loop through pyramid layers
    rpn_class_logits, rpn_class, rpn_bbox = rpn(rpn_feature_maps)  # list of lists
    proposal_count = Config.POST_NMS_ROIS_TRAINING
    rpn_rois = ProposalLayer(
        proposal_count=proposal_count,
        nms_threshold=Config.RPN_NMS_THRESHOLD,
        name="ROI"
    )([rpn_class, rpn_bbox, anchors])
    target_rois = rpn_rois

    rois, target_class_ids, target_bbox, target_mask = \
        DetectionTargetLayer(Config, name="proposal_targets")([
            target_rois, input_gt_class_ids, gt_boxes, input_gt_masks])
    shared_feature = ROIAlign(rois, mrcnn_feature_maps,
                                    Config.POOL_SIZE, Config.NUM_CLASSES,
                                    train_bn=Config.TRAIN_BN,
                                    fc_layers_size=Config.FPN_CLASSIF_FC_LAYERS_SIZE)
    mrcnn_class_logits, mrcnn_class, mrcnn_bbox = \
        fpn_classifier_graph(shared_feature,
                                    Config.POOL_SIZE, Config.NUM_CLASSES,
                                    train_bn=Config.TRAIN_BN,
                                    fc_layers_size=Config.FPN_CLASSIF_FC_LAYERS_SIZE)
    mrcnn_mask = build_fpn_mask_graph(shared_feature,
                                             Config.MASK_POOL_SIZE,
                                             Config.NUM_CLASSES,
                                             train_bn=Config.TRAIN_BN)
    output_rois = KL.Lambda(lambda x: x * 1, name="output_rois")(rois)
    rpn_class_loss = KL.Lambda(lambda x: rpn_class_loss_graph(*x), name="rpn_class_loss")(
        [input_rpn_match, rpn_class_logits])
    rpn_bbox_loss = KL.Lambda(lambda x: rpn_bbox_loss_graph(Config, *x), name="rpn_bbox_loss")(
        [input_rpn_bbox, input_rpn_match, rpn_bbox])
    class_loss = KL.Lambda(lambda x: mrcnn_class_loss_graph(*x), name="mrcnn_class_loss")(
        [target_class_ids, mrcnn_class_logits, active_class_ids])
    bbox_loss = KL.Lambda(lambda x: mrcnn_bbox_loss_graph(*x), name="mrcnn_bbox_loss")(
        [target_bbox, target_class_ids, mrcnn_bbox])
    mask_loss = KL.Lambda(lambda x: mrcnn_mask_loss_graph(*x), name="mrcnn_mask_loss")(
        [target_mask, target_class_ids, mrcnn_mask])
    inputs = [input_image, input_anchors,
              input_rpn_match, input_rpn_bbox, input_gt_class_ids, input_gt_boxes, input_gt_masks, active_class_ids]
    outputs = [rpn_class_logits, rpn_class, rpn_bbox,
               mrcnn_class_logits, mrcnn_class, mrcnn_bbox, mrcnn_mask,
               rpn_rois, output_rois,
               rpn_class_loss, rpn_bbox_loss, class_loss, bbox_loss, mask_loss]
    model = Model(inputs, outputs, name='mask_rcnn')

    optimizer = SGD(lr=0.0001, momentum=0.9999, clipnorm=5.0)

    # Add Losses
    loss_names = ["rpn_class_loss", "rpn_bbox_loss", "mrcnn_class_loss", "mrcnn_bbox_loss", "mrcnn_mask_loss"]
    for name in loss_names:
        layer = model.get_layer(name) # redundant; leave it unchanged
        loss = tf.reduce_mean(layer.output, keepdims=True) * LOSS_WEIGHTS.get(name, 1.) # redundant; leave it unchanged
        model.add_loss(loss)
    model.compile(optimizer=optimizer, loss=[None] * len(model.outputs))


    model.fit_generator(
        train_generator,
        epochs=10,
        steps_per_epoch=500,
        validation_data=val_generator,
        validation_steps=20,
        max_queue_size=15,
        use_multiprocessing=True,
        callbacks=callbacks,
        initial_epoch=0)

    # TODO: save necessary files, such as weights, model, loss_history etc
    model.save_model('model.hd5')
    model.save_weights('final_weight.hd5')
    # Anchors in normalized coordinates


############################################################
#  Detection Layer
############################################################

def refine_detections_graph(rois, probs, deltas, config):
    """Refine classified proposals and filter overlaps and return final
    detections.

    Inputs:
        rois: [N, (z1, y1, x1, z2, y2, x2)] in normalized coordinates
        probs: [N, num_classes]. Class probabilities.
        deltas: [N, num_classes, (dz, dy, dx, log(dd), log(dh), log(dw))]. Class-specific
                bounding box deltas.
        window: (z1, y1, x1, z2, y2, x2) in image coordinates. The part of the image
            that contains the image excluding the padding.

    Returns detections shaped: [N, (z1, y1, x1, z2, y2, x2, class_id, score)] where
        coordinates are normalized.
    """
    # print(rois)
    # print(centers)
    # Class IDs per ROI
    class_ids = tf.argmax(probs, axis=1, output_type=tf.int32)
    # Class probability of the top class of each ROI
    indices = tf.stack([tf.range(probs.shape[0]), class_ids], axis=1)
    class_scores = tf.gather_nd(probs, indices)
    # Class-specific bounding box deltas
    deltas_specific = tf.gather_nd(deltas, indices)
    # Apply bounding box deltas
    # Shape: [boxes, (y1, x1, y2, x2)] in normalized coordinates
    refined_rois = utils.apply_box_deltas_graph(
        rois, deltas_specific * config.BBOX_STD_DEV)
    window = np.array([0, 0, 0, 1, 1, 1], dtype=np.float32)

    # Clip boxes to image window
    #refined_rois = utils.clip_boxes_graph(refined_rois, window)

    # TODO: Filter out boxes with zero area

    # Filter out background boxes
    keep = tf.where(class_ids > 0)[:, 0]
    # Filter out low confidence boxes
    if config.DETECTION_MIN_CONFIDENCE:
        conf_keep = tf.where(class_scores >= config.DETECTION_MIN_CONFIDENCE)[:, 0]
        keep = tf.sets.set_intersection(tf.expand_dims(keep, 0),
                                        tf.expand_dims(conf_keep, 0))
        keep = tf.sparse_tensor_to_dense(keep)[0]

    # Apply per-class NMS
    # 1. Prepare variables
    pre_nms_class_ids = tf.gather(class_ids, keep)
    pre_nms_scores = tf.gather(class_scores, keep)
    pre_nms_rois = tf.gather(refined_rois, keep)
    unique_pre_nms_class_ids = tf.unique(pre_nms_class_ids)[0]

    def nms_keep_map(class_id):
        """Apply Non-Maximum Suppression on ROIs of the given class."""
        # Indices of ROIs of the given class
        ixs = tf.where(tf.equal(pre_nms_class_ids, class_id))[:, 0]
        # Apply NMS
        box_opt = tf.load_op_library('/home/yankun/tensorflow/CR/nms.so')

        # indices = box_opt.non_max(
        #     boxes, scores, self.proposal_count,
        #     self.nms_threshold, name="rpn_non_max_suppression")
        class_keep =  box_opt.non_max(
            tf.gather(pre_nms_rois, ixs),
            tf.gather(pre_nms_scores, ixs),
            max_output_size=config.DETECTION_MAX_INSTANCES,
            iou_threshold=config.DETECTION_NMS_THRESHOLD)
        # Map indicies
        class_keep = tf.gather(keep, tf.gather(ixs, class_keep))
        # Pad with -1 so returned tensors have the same shape
        gap = config.DETECTION_MAX_INSTANCES - tf.shape(class_keep)[0]
        class_keep = tf.pad(class_keep, [(0, gap)],
                            mode='CONSTANT', constant_values=-1)
        # Set shape so map_fn() can infer result shape
        class_keep.set_shape([config.DETECTION_MAX_INSTANCES])
        return class_keep

    # 2. Map over class IDs
    nms_keep = tf.map_fn(nms_keep_map, unique_pre_nms_class_ids,
                         dtype=tf.int64)
    # 3. Merge results into one list, and remove -1 padding
    nms_keep = tf.reshape(nms_keep, [-1])
    nms_keep = tf.gather(nms_keep, tf.where(nms_keep > -1)[:, 0])
    # 4. Compute intersection between keep and nms_keep
    keep = tf.sets.set_intersection(tf.expand_dims(keep, 0),
                                    tf.expand_dims(nms_keep, 0))
    keep = tf.sparse_tensor_to_dense(keep)[0]
    # Keep top detections
    roi_count = config.DETECTION_MAX_INSTANCES
    class_scores_keep = tf.gather(class_scores, keep)
    num_keep = tf.minimum(tf.shape(class_scores_keep)[0], roi_count)
    top_ids = tf.nn.top_k(class_scores_keep, k=num_keep, sorted=True)[1]
    keep = tf.gather(keep, top_ids)

    # Arrange output as [N, (y1, x1, y2, x2, class_id, score)]
    # Coordinates are normalized.
    detections = tf.concat([
        tf.gather(refined_rois, keep),
        tf.to_float(tf.gather(class_ids, keep))[..., tf.newaxis],
        tf.gather(class_scores, keep)[..., tf.newaxis]
    ], axis=1)
    # Pad with zeros if detections < DETECTION_MAX_INSTANCES
    gap = config.DETECTION_MAX_INSTANCES - tf.shape(detections)[0]
    detections = tf.pad(detections, [(0, gap), (0, 0)], "CONSTANT")

    return detections


class DetectionLayer(KE.Layer):
    """Takes classified proposal boxes and their bounding box deltas and
    returns the final detection boxes.

    Returns:
    [batch, num_detections, (y1, x1, y2, x2, class_id, class_score)] where
    coordinates are normalized.
    """

    def __init__(self, config=None, **kwargs):
        super(DetectionLayer, self).__init__(**kwargs)
        self.config = config

    def call(self, inputs):
        rois = inputs[0]
        mrcnn_class = inputs[1]
        mrcnn_bbox = inputs[2]
        # print(rois)
        # print(mrcnn_center)
        # Get windows of images in normalized coordinates. Windows are the area
        # in the image that excludes the padding.
        # Use the shape of the first image in the batch to normalize the window
        # because we know that all images get resized to the same size.
        #m = parse_image_meta_graph(image_meta)
        #image_shape = tf.Variable(np.array([120,144,144]))     #[120,144,144]

        # Run detection refinement graph on each item in the batch
        detections_batch = utils.batch_slice(
            [rois, mrcnn_class, mrcnn_bbox],
            lambda x, y, w: refine_detections_graph(x, y, w, self.config),
            self.config.IMAGES_PER_GPU)

        # Reshape output
        # [batch, num_detections, (y1, x1, y2, x2, class_score)] in
        # normalized coordinates
        return tf.reshape(
            detections_batch,
            [1, self.config.DETECTION_MAX_INSTANCES, 8])

    def compute_output_shape(self, input_shape):
        return (None, self.config.DETECTION_MAX_INSTANCES, 8)


