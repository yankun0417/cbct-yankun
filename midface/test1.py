import sys
from config import Config
import model as modellib
import numpy as np
import utils
import utils
import module
import scipy.ndimage as nd
import math
import SimpleITK as sitk
import math
import os
import time
from random import randint, choice
from keras.models import Model
from keras.layers import Input, Lambda, Concatenate, Add, TimeDistributed
from keras.regularizers import l2
from keras.callbacks import LearningRateScheduler, ReduceLROnPlateau, ModelCheckpoint, TensorBoard, Callback
from keras.layers.core import Dense, Activation, Dropout, Flatten, Reshape
from keras.layers.convolutional import Conv3D, UpSampling3D, ZeroPadding3D
import keras.layers as KL
import keras.engine as KE
import keras.models as KM
import h5py
from keras.layers.pooling import MaxPooling3D, AveragePooling3D
from keras.layers.normalization import BatchNormalization
from keras.layers.merge import concatenate
from keras.optimizers import SGD, RMSprop, Adadelta, Adam
from keras import backend as K
import tensorflow as tf
def read_image(file_image, file_index,common_spacing):
    image = sitk.ReadImage(file_image)
    spacing = image.GetSpacing()
    image_array = sitk.GetArrayFromImage(image)



    norm_ratio = spacing / common_spacing
    norm_ratio = norm_ratio[::-1]
    #################### change resolution ############################
    SN_image_array = nd.interpolation.zoom(image_array, norm_ratio, order=1)
    rescale_image = rescale(SN_image_array)
    if file_index == 1.6:
        f = h5py.File('./HM_Rescaled_subject_1.hdf5', 'r')
        image_match = f['image'].value
    elif file_index == 0.4:
        f = h5py.File(Config.path_04, 'r')
        image_match = f['image'].value
    matched_image = histogram(image_match, rescale_image,common_spacing)

    return matched_image, norm_ratio,spacing#, ds_seg


def rescale(image):
    minimum = np.min(image)
    maximum = np.max(image)
    intensity_range = maximum - minimum
    scale = intensity_range / (1.0 * 4095)
    scaled_image_array = (np.floor((image - minimum) / scale)).astype(np.float32)
    return scaled_image_array


def histogram(image_match, image,common_spacing):
    R = sitk.GetImageFromArray(image_match)
    R.SetSpacing(common_spacing)
    I = sitk.GetImageFromArray(image)
    I.SetSpacing(common_spacing)
    NI = sitk.HistogramMatching(I, R)
    image_array = sitk.GetArrayFromImage(NI)
    return image_array
def test(argv):
    arg = sys.argv[1]
    #seg_path = sys.argv[2]  # for visualizaiton
    save_path = sys.argv[2]
    current_directory = os.getcwd()
    start_time = time.time()
    file_image = current_directory + '/' + arg
    #file_segmentation = current_directory + '/' + seg_path  # vis
    number_of_lmk = 33
    res = 1.6
    common_spacing = np.array([res, res, res])
    model = modellib.MaskRCNN(mode="inference", config=Config,
                              model_dir='/home/yankun/tensorflow')
    image_array, norm_ratio, spacing = read_image(file_image, res, common_spacing)  # vis

    ##image_path = '/home/yankun/original_images/train/1.6mm/RC_HM_image/'
    #seg_path = '/home/yankun/original_images/train/1.6mm/segmentation/'
    #lmk_path = '/home/yankun/data/train/1.6mm/landmarks/'
    #heatmap_path  = '/home/yankun/keras-gcn-master/kegra/heatmap_0-30/'
    #train_list1 = np.array([3,5,6,7,15,17,19,20,21,25,26,27,30,31,32,34,35,36,40,43,44,46,49,50,53,55,57,59,65,66,67,68,70]) #2,4,5,6,7,8,9,10,11,12,13,14,15,22,
    #                        24,26,30,31,32,36,38,41,42,44,45,
    #                        48,49,50,51,52,53,54,55,56,57,58,60,52,53,64,75,83,65,,60,62,63,64,75,83,65
    #original_image_path = '/home/yankun/original_images/'
    np.array([0, 1, 1, 2, 3, 2, 3, 1, 1, 1, 1, 2, 3, 2, 3, 0, 0, 0, 0, 0, 2,
              2, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4,
              4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
              5, 5, 5, 5, 5, 5, 5,
              7, 7, 8, 8, 7, 8, 7, 8, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8,
              9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9]
             )
    region1 = [0,15,16,17,18,19,22,25,26,27,28,29,30,31,32]
    region2 = [1,2,7,8,9,10]
    region3 = [3,5,11,13,20,21]
    region4 = [4,6,12,14,23,24]
    mse_r1 = 0.0
    mse_r2 = 0.0
    mse_r3 = 0.0
    mse_r4 = 0.0
    mse_total1 = 0.0
    #image_idx = train_list1[i]
    final_landmark = open(arg + '_final_landmark.txt', 'a')



    padded_image_size = np.array([128, 128, 128])

    bias_shape = padded_image_size - image_array.shape
    padded_image = np.zeros(padded_image_size, dtype=np.float32)
    if bias_shape[0] < 0:
        p_z_s = 0
        p_z_stop = padded_image_size[0]
        i_z_s = -bias_shape[0] / 2
        i_z_stop = -bias_shape[0] / 2 + padded_image_size[0]
    else:
        p_z_s = bias_shape[0] / 2
        p_z_stop = bias_shape[0] / 2 + image_array.shape[0]
        i_z_s = 0
        i_z_stop = image_array.shape[0]
    if bias_shape[1] < 0:
        p_y_s = 0
        p_y_stop = padded_image_size[1]
        i_y_s = -bias_shape[1] / 2
        i_y_stop = -bias_shape[1] / 2 + padded_image_size[1]
    else:
        p_y_s = bias_shape[1] / 2
        p_y_stop = bias_shape[1] / 2 + image_array.shape[1]
        i_y_s = 0
        i_y_stop = image_array.shape[1]
    if bias_shape[2] < 0:
        p_x_s = 0
        p_x_stop = padded_image_size[2]
        i_x_s = -bias_shape[2] / 2
        i_x_stop = -bias_shape[2] / 2 + padded_image_size[2]
    else:
        p_x_s = bias_shape[2] / 2
        p_x_stop = bias_shape[2] / 2 + image_array.shape[2]
        i_x_s = 0
        i_x_stop = image_array.shape[2]
    padded_image[int(p_z_s):int(p_z_stop),
    int(p_y_s):int(p_y_stop),
    int(p_x_s):int(p_x_stop)] = image_array[int(i_z_s):int(i_z_stop), int(i_y_s):int(i_y_stop),
                                int(i_x_s):int(i_x_stop)] / 4096.0
    # padded_image[int(bias_shape[0]):int(bias_shape[0]) + int(image_array.shape[0]),
    # int(bias_shape[1]):int(bias_shape[1]) + int(image_array.shape[1]),
    # int(bias_shape[2]):int(bias_shape[2]) + int(image_array.shape[2])] = image_array/4096.0



    class_id = np.ones((number_of_lmk, 1), dtype=np.int32)
    # lmk1 = lmk[0:33]

    # for i in range(number_of_lmk):
    #     if int(lmk1[i][0]) < 1:
    #         class_id[i][0] = 0
    #     else:
    #         class_id[i][0] = 1
    # lmk = lmk1 + bias_shape/2
    batch_size = 1
    batch_images = np.zeros((batch_size,) + padded_image.shape, dtype=np.float32)
    # batch_lmk = np.zeros((batch_size,) + lmk.shape, dtype=np.float32)
    batch_images[0] = padded_image.astype(np.float32)
    image_in = np.expand_dims(batch_images, axis=0)

    class_id = np.expand_dims(class_id, axis=0)
    # batch_lmk[0] = lmk
    r = model.detect(image_in)
    # print(r)
    lmk_p = r[0]["heatmap"]
    cls_p = r[0]['cls']
    class_p = np.argmax(cls_p, axis=1)

    mse_total = 0.0
    mse_total_r1 = 0.0
    mse_total_r2 = 0.0
    mse_total_r3 = 0.0
    mse_total_r4 = 0.0

    lmk_predict = np.zeros((33, 3), dtype=np.float32)
    for i in range(lmk_p.shape[0]):
        # print(i)
        # print(lmk_p[i].max())
        lmk_cor_p = np.unravel_index(np.argmax(lmk_p[i], axis=None), lmk_p[i].shape)
        # print(lmk_cor_p)
        # print(np.unravel_index(np.argmax(lmk_p[i], axis=None), lmk_p[i].shape))
        # print(int(lmk[i][0]), int(lmk[i][1]), int(lmk[i][2]))
        # print(int(lmk_cor_p[i][0]),int(lmk_cor_p[i][1]),int(lmk_cor_p[i][2]))
        common_spacing = np.array([1.6, 1.6, 1.6])
        # SN_seg = sitk.GetImageFromArray(lmk_p[i])
        # SN_seg.SetSpacing(common_spacing)
        # sitk.WriteImage(SN_seg, 'heatmap_{}.nii.gz'.format(i))
        # z_p = int((lmk_cor_p[i] * padded_image_size )[0]) * 2
        # y_p = int((lmk_cor_p[i] * padded_image_size )[1]) * 2
        # x_p = int((lmk_cor_p[i] * padded_image_size )[2]) * 2
        lmk_predict[i][0] = lmk_cor_p[0]
        lmk_predict[i][1] = lmk_cor_p[1]
        lmk_predict[i][2] = lmk_cor_p[2]
        z_p = int((lmk_cor_p)[0]) * 2
        y_p = int((lmk_cor_p)[1]) * 2
        x_p = int((lmk_cor_p)[2]) * 2
        # mse = (lmk[i][0] - lmk_cor_p[0]) * (lmk[i][0] - lmk_cor_p[0]) + \
        #       (lmk[i][1] - lmk_cor_p[1]) * (lmk[i][1] - lmk_cor_p[1]) + \
        #       (lmk[i][2] - lmk_cor_p[2]) * (lmk[i][2] - lmk_cor_p[2])
        # mse_total = mse_total + math.sqrt(mse)
        # print(z_p / 2, y_p / 2, x_p / 2)
        # z_gt = int(lmk[i][0]) * 2
        # y_gt = int(lmk[i][1]) * 2
        # x_gt = int(lmk[i][2]) * 2
        if class_p[i] == 1:
            bia_z = np.random.choice([-1, 0, 1], size=1, replace=False)
            bia_y = np.random.choice([-1, 0, 1], size=1, replace=False)
            bia_x = np.random.choice([-1, 0, 1], size=1, replace=False)
            z_high = int((lmk_cor_p)[0]) - bias_shape[0] / 2
            y_high = int((lmk_cor_p)[1]) - bias_shape[1] / 2
            x_high = int((lmk_cor_p)[2]) - bias_shape[2] / 2
            z_high1 = int(z_high * 1.6 / spacing[2])
            y_high1 = int(y_high * 1.6 / spacing[1])
            x_high1 = int(x_high * 1.6 / spacing[0])
            print(z_high1, y_high1, x_high1)
            final_landmark.write(str(x_high1 * spacing[0]))
            final_landmark.write(' ')
            final_landmark.write(str(y_high1 * spacing[1]))
            final_landmark.write(' ')
            final_landmark.write(str(z_high1 * spacing[2]))
            final_landmark.write(' ')
            final_landmark.write('\n')
        else:
            final_landmark.write(str(0.0))
            final_landmark.write(' ')
            final_landmark.write(str(0.0))
            final_landmark.write(' ')
            final_landmark.write(str(0.0))
            final_landmark.write(' ')
            final_landmark.write('\n')
        # gt_landmark.write(str(x_gt_high1 * spacing[0]))
        # gt_landmark.write(' ')
        # gt_landmark.write(str(y_gt_high1 * spacing[1]))
        # gt_landmark.write(' ')
        # gt_landmark.write(str(z_gt_high1 * spacing[2]))
        # gt_landmark.write(' ')
        # gt_landmark.write('\n')
       # midface_ori_array[z_high1 - 1:z_high1 + 1, y_high1 - 3:y_high1 + 3, x_high1 - 3:x_high1 + 3] = i


if __name__ == '__main__':
    test(sys.argv)