from config import Config
import model as modellib
import numpy as np
import utils
import utils
import module
from keras.models import Model
from keras.layers import Input, Lambda, Concatenate, Add, TimeDistributed
from keras.regularizers import l2
from keras.callbacks import LearningRateScheduler, ReduceLROnPlateau, ModelCheckpoint, TensorBoard, Callback
from keras.layers.core import Dense, Activation, Dropout, Flatten, Reshape
from keras.layers.convolutional import Conv3D, UpSampling3D, ZeroPadding3D
import keras.layers as KL
import keras.engine as KE
import keras.models as KM
import h5py
from keras.layers.pooling import MaxPooling3D, AveragePooling3D
from keras.layers.normalization import BatchNormalization
from keras.layers.merge import concatenate
from keras.optimizers import SGD, RMSprop, Adadelta, Adam
from keras import backend as K
import tensorflow as tf


model = modellib.MaskRCNN(mode="training", config=Config,
                                  model_dir='/home/yankun/tensorflow')
#model.load_weights('/home/yankun/tensorflow/utils/model_30.best.hd5')
# r = model.detect(padded_image)
# class_ids = r[0]['class_ids']
# b_gt = bbox_gt[class_ids]
# p_gt = r[0]['rois']
# #c_gt = r[0]['centers']
# print(class_ids)
# print(r[0]['rois'])
# print(b_gt)
# location_gt = np.zeros([len(class_ids), 3], dtype = np.float32)
# location_p = np.zeros([len(class_ids), 3], dtype = np.float32)
#
# for i in range(len(class_ids)):
#     z_gt = np.int32(b_gt[i][0] + ((b_gt[i][3] - b_gt[i][0]) / 2.0))
#     y_gt = np.int32(b_gt[i][1] + ((b_gt[i][4] - b_gt[i][1]) / 2.0))
#     x_gt = np.int32(b_gt[i][2] + ((b_gt[i][5] - b_gt[i][2]) / 2.0))
#     z_p = np.int32(p_gt[i][0] + ((p_gt[i][3] - p_gt[i][0]) / 2.0))
#     y_p = np.int32(p_gt[i][1] + ((p_gt[i][4] - p_gt[i][1]) / 2.0))
#     x_p = np.int32(p_gt[i][2] + ((p_gt[i][5] - p_gt[i][2]) / 2.0))
#     location_gt[i] = [z_gt, y_gt, x_gt]
#     location_p[i] = [z_p, y_p, x_p]
#
#
#     # seg_GT[z_gt - 3:z_gt + 3, y_gt - 3:y_gt + 3, x_gt - 3:x_gt + 3] = i + 1
#     # result = nib.Nifti1Image(seg_GT, np.eye(4))
#     # nib.save(result, "seg_lmk_gt_21.nii.gz")
#     # #print(x, y, z, location[i + 25])
#     #
#     # seg_image[z_p - 3:z_p + 3, y_p - 3:y_p + 3, x_p - 3:x_p + 3] = i + 1
#     # result = nib.Nifti1Image(seg_image, np.eye(4))
#     # nib.save(result, "seg_lmk_21.nii.gz")
#     #
#     # result = nib.Nifti1Image(padded_image, np.eye(4))
#     # nib.save(result, "image_21.nii.gz")
# print(location_gt)
# print(location_p)
train_list1 = np.arange(1,150)
#train_list = np.concatenate((train_list1, train_list2))
#print(train_list.shape)
validation_list = np.arange(150,200)
learning_rate = 0.01
epoch = 20
model.train(train_list1, validation_list, learning_rate, epoch)
print('done')
# if __name__ == '__main__':
#     print('111')
#     train()