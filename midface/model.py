import utils
import module
import numpy as np
import tensorflow as tf
import keras
import keras.backend as K
import keras.layers as KL
import keras.engine as KE
import keras.models as KM
from keras.regularizers import l2
from keras.layers import Input, Dropout
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Dense, Activation, Dropout, Flatten, Reshape
import os
from keras.layers import Input, Lambda, Concatenate, Add, TimeDistributed
from keras.callbacks import LearningRateScheduler, ReduceLROnPlateau, ModelCheckpoint, TensorBoard, Callback


class MaskRCNN():
    """Encapsulates the Mask RCNN model functionality.

    The actual Keras model is in the keras_model property.
    """

    def __init__(self, mode, config, model_dir):
        """
        mode: Either "training" or "inference"
        config: A Sub-class of the Config class
        model_dir: Directory to save training logs and trained weights
        """
        assert mode in ['training', 'inference']
        self.mode = mode
        self.config = config
        self.model_dir = model_dir
        #self.set_log_dir()
        self.keras_model = self.build(mode=mode, config=config)

    def build(self, mode, config):
        """Build Mask R-CNN architecture.
            input_shape: The shape of the input image.
            mode: Either "training" or "inference". The inputs and
                outputs of the model differ accordingly.
        """
        assert mode in ['training', 'inference']
        number_of_lmk = 33
        IMAGE_SHAPE = [128, 128, 128]

        # Inputs
        input_image = KL.Input(
            shape=[1, IMAGE_SHAPE[0], IMAGE_SHAPE[1], IMAGE_SHAPE[2]], name="input_image", dtype=tf.float32)

        if mode == "training":
            # RPN GT
            input_class_id = KL.Input(
                shape=[None, 1], name="input_class_id", dtype=tf.int32)
            input_lmk = KL.Input(
                shape=[None, 3], name="input_lmk", dtype=tf.float32)
            input_heatmap = KL.Input(shape = [number_of_lmk, IMAGE_SHAPE[0], IMAGE_SHAPE[1], IMAGE_SHAPE[2]], name="input_heatmap", dtype=tf.float32)


        C1,C2,C3,C4,C5 = module.resnet_graph(input_image)
        up_pooling = module.expand_graph(C1,C2,C3,C4,C5)

        # Note that P6 is used in RPN, but not in the classifier heads.
        #rpn_feature_maps = C5

        # RPN Model
        rpn = module.build_rpn_model(number_of_lmk)

        # Loop through pyramid layers
        #rpn_class_logits, rpn_class, rpn_bbox = rpn(up_pooling)  # list of lists
        heatmap = rpn(up_pooling)  # list of lists
        up_pooling1 = KL.Lambda(lambda t: tf.reshape(t, [1, 128 * 128 * 128,32]))(up_pooling)
        print(up_pooling1)
        heatmap1 = KL.Lambda(lambda t: tf.reshape(t, [1, 128*128*128,number_of_lmk]))(heatmap)
        print(heatmap1)
        features = KL.dot([heatmap1, up_pooling1],axes = [1,1],normalize = True)
        print(features)
        # features = KL.Lambda(lambda t: tf.reshape(t, [1, number_of_lmk*32]))(features)
        features = BatchNormalization(axis = 1)(features)


        Y1 = KL.TimeDistributed(
            KL.Dense(32, kernel_initializer='he_normal', kernel_regularizer=l2(5e-4)))(features)
        Y1 = BatchNormalization(axis = 1)(Y1)
        Y1 = KL.TimeDistributed(
            Activation('relu'))(Y1)

        Y1 = KL.TimeDistributed(KL.Dense(2, activation='softmax'), name='class_loss')(Y1)

        if mode == "training":
            # Losses

            heatmap_regression_loss = KL.Lambda(lambda x: module.rpn_bbox_loss_graph2(*x), name="rpn_b_loss")(
                [input_heatmap, heatmap])
            # lmk_regression_loss = KL.Lambda(lambda x: module.lmk_regression_loss_graph2(*x), name="lmk_b_loss")(
            #     [input_lmk, Y2])
            class_loss = KL.Lambda(lambda x: module.class_loss_graph(*x), name="c_loss")(
                [input_class_id, Y1])

            # Model
            inputs = [input_image,
                      input_class_id, input_lmk,  input_heatmap]

            outputs = [heatmap,heatmap_regression_loss,Y1,class_loss, ] #'center_loss'Y2,lmk_regression_loss,,Y1,class_loss
            model = KM.Model(inputs, outputs, name='mask_rcnn')
            #
        else:
            # Network Heads
            # Proposal classifier and BBox regressor heads
            # shared_feature = module.ROIAlign(rpn_rois, mrcnn_feature_maps,
            #                                  config.POOL_SIZE, config.NUM_CLASSES,
            #                                  train_bn=config.TRAIN_BN,
            #                                  fc_layers_size=config.FPN_CLASSIF_FC_LAYERS_SIZE)
            # mrcnn_class_logits, mrcnn_class, mrcnn_bbox = \
            #     module.fpn_classifier_graph(shared_feature,
            #                                 config.POOL_SIZE, config.NUM_CLASSES,
            #                                 train_bn=config.TRAIN_BN,
            #                                 fc_layers_size=config.FPN_CLASSIF_FC_LAYERS_SIZE)
            # #print(mrcnn_center, rpn_rois)
            # # Detections
            # # output is [batch, num_detections, (z1, y1, x1, z2, y2, x2, class_id, score)] in
            # # normalized coordinates
            # detections = module.DetectionLayer(config, name="mrcnn_detection")(
            #     [rpn_rois, mrcnn_class, mrcnn_bbox])
            #
            # # Create masks for detections
            # detection_boxes = KL.Lambda(lambda x: x[..., :6])(detections)
            # shared_feature_mask = module.ROIAlign(detection_boxes, mrcnn_feature_maps,
            #                                  config.POOL_SIZE, config.NUM_CLASSES,
            #                                  train_bn=config.TRAIN_BN,
            #                                  fc_layers_size=config.FPN_CLASSIF_FC_LAYERS_SIZE)
            # #print(shared_feature_mask)
            # mrcnn_mask = module.build_fpn_mask_graph(shared_feature_mask,
            #                                   config.MASK_POOL_SIZE,
            #                                   config.NUM_CLASSES,
            #                                   train_bn=config.TRAIN_BN)
            model = KM.Model([input_image],

                            [heatmap,Y1],   #,Y2,
                             name='mask_rcnn')
            #print(model.summary())
            # model2 = KM.Model([input_image, input_anchors],
            #                  outputs=model.layers[10].output, # 10-> 1/2, 20->1/4, 28->1/8
            #                  #[rpn_class_logits, rpn_class, rpn_bbox],
            #                  name='mask_rcnn')
        #print(model.summary())
        #model.summary()
        model.load_weights('./1st_weights.hd5', by_name=True)
        #model.load_weights('dental_weights.15.hd5', by_name=True)

        #model2.load_weights('/home/yankun/CBCT patient image 105/data_lang/1.6mm/CBCT_RPN_0_31.best.hd5', by_name=True)
        # Add multi-GPU support.
        # if config.GPU_COUNT > 1:
        #     from mrcnn.parallel_model import ParallelModel
        #     model = ParallelModel(model, config.GPU_COUNT)

        return  model

    def get_anchors(self, image_shape):
        """Returns anchor pyramid for the given image size."""
        config = self.config
        backbone_shapes = np.array([144, 192, 192]) // config.BACKBONE_STRIDES        # Cache anchors and reuse if image shape is the same
        if not hasattr(self, "_anchor_cache"):
            self._anchor_cache = {}
        if not tuple(image_shape) in self._anchor_cache:
            # Generate Anchors
            a = utils.generate_pyramid_anchors(
                self.config.RPN_ANCHOR_SCALES,
                self.config.RPN_ANCHOR_RATIOS,
                backbone_shapes,
                self.config.BACKBONE_STRIDES,
                self.config.RPN_ANCHOR_STRIDE)
            # Keep a copy of the latest anchors in pixel coordinates because
            # it's used in inspect_model notebooks.
            # TODO: Remove this after the notebook are refactored to not use it
            self.anchors = a
            # Normalize coordinates
            self._anchor_cache[tuple(image_shape)] = utils.norm_boxes(a, image_shape[:3])
        return self._anchor_cache[tuple(image_shape)]

    def compile(self, learning_rate, momentum):
        """Gets the model ready for training. Adds losses, regularization, and
        metrics. Then calls the Keras compile() function.
        """
        # Optimizer object
        optimizer = keras.optimizers.SGD(
            lr=learning_rate, momentum=momentum,
            clipnorm=self.config.GRADIENT_CLIP_NORM)
        # Add Losses
        # First, clear previously set losses to avoid duplication
        self.keras_model._losses = []
        self.keras_model._per_input_losses = {}
        loss_names = [
              "rpn_b_loss","c_loss"] #"center_loss","rpn_c_loss",'lmk_b_loss',#
        for name in loss_names:
            layer = self.keras_model.get_layer(name)
            if layer.output in self.keras_model.losses:
                continue
            loss = (
                tf.reduce_mean(layer.output, keepdims=True))
                # * self.config.LOSS_WEIGHTS.get(name, 1.))
            self.keras_model.add_loss(loss)

        # Add L2 Regularization
        # Skip gamma and beta weights of batch normalization layers.
        reg_losses = [
            keras.regularizers.l2(self.config.WEIGHT_DECAY)(w) / tf.cast(tf.size(w), tf.float32)
            for w in self.keras_model.trainable_weights
            if 'gamma' not in w.name and 'beta' not in w.name]
        self.keras_model.add_loss(tf.add_n(reg_losses))

        # Compile
        self.keras_model.compile(
            optimizer=optimizer,
            loss=[None] * len(self.keras_model.outputs))

        # Add metrics for losses
        for name in loss_names:
            if name in self.keras_model.metrics_names:
                continue
            layer = self.keras_model.get_layer(name)
            self.keras_model.metrics_names.append(name)
            loss = (
                tf.reduce_mean(layer.output, keepdims=True))
                # * self.config.LOSS_WEIGHTS.get(name, 1.))
            self.keras_model.metrics_tensors.append(loss)


    def load_weights(self, filepath, by_name=False, exclude=None):
        """Modified version of the correspoding Keras function with
        the addition of multi-GPU support and the ability to exclude
        some layers from loading.
        exlude: list of layer names to excluce
        """
        import h5py
        from keras.engine import topology



        if exclude:
            by_name = True

        if h5py is None:
            raise ImportError('`load_weights` requires h5py.')
        f = h5py.File(filepath, mode='r')
        if 'layer_names' not in f.attrs and 'model_weights' in f:
            f = f['model_weights']

        # In multi-GPU training, we wrap the model. Get layers
        # of the inner model because they have the weights.
        keras_model = self.keras_model
        layers = keras_model.inner_model.layers if hasattr(keras_model, "inner_model")\
            else keras_model.layers

        # Exclude some layers
        if exclude:
            layers = filter(lambda l: l.name not in exclude, layers)

        if by_name:
            topology.load_weights_from_hdf5_group_by_name(f, layers)
        else:
            topology.load_weights_from_hdf5_group(f, layers)
        if hasattr(f, 'close'):
            f.close()
    def train(self, train_dataset, val_dataset, learning_rate, epochs,
              augmentation=None):
        """Train the model.
        train_dataset, val_dataset: Training and validation Dataset objects.
        learning_rate: The learning rate to train with
        epochs: Number of training epochs. Note that previous training epochs
                are considered to be done alreay, so this actually determines
                the epochs to train in total rather than in this particaular
                call.
        layers: Allows selecting wich layers to train. It can be:
            - A regular expression to match layer names to train
            - One of these predefined values:
              heads: The RPN, classifier and mask heads of the network
              all: All the layers
              3+: Train Resnet stage 3 and up
              4+: Train Resnet stage 4 and up
              5+: Train Resnet stage 5 and up
        augmentation: Optional. An imgaug (https://github.com/aleju/imgaug)
            augmentation. For example, passing imgaug.augmenters.Fliplr(0.5)
            flips images right/left 50% of the time. You can pass complex
            augmentations as well. This augmentation applies 50% of the
            time, and when it does it flips images right/left half the time
            and adds a Gausssian blur with a random sigma in range 0 to 5.

                augmentation = imgaug.augmenters.Sometimes(0.5, [
                    imgaug.augmenters.Fliplr(0.5),
                    imgaug.augmenters.GaussianBlur(sigma=(0.0, 5.0))
                ])
        """
        assert self.mode == "training", "Create model in training mode."

        # Data generators
        train_generator = utils.data_generator(train_dataset, self.config, shuffle=True,
                                         augmentation=augmentation,
                                         batch_size=1)
        val_generator = utils.data_generator(val_dataset, self.config, shuffle=True,
                                       batch_size=1)

        # Callbacks


        callbacks = [keras.callbacks.TensorBoard(log_dir= os.getcwd() + '/',
                                        histogram_freq=0, write_graph=True, write_images=False),
                     ModelCheckpoint('1st_weights.hd5', verbose=0, save_weights_only=True)]  # TODO

        # Train

        self.compile(learning_rate, self.config.LEARNING_MOMENTUM)

        # Work-around for Windows: Keras fails on Windows when using
        # multiprocessing workers. See discussion here:
        # https://github.com/matterport/Mask_RCNN/issues/13#issuecomment-353124009

        self.keras_model.fit_generator(
            train_generator,
            initial_epoch=0,
            epochs=epochs,
            steps_per_epoch=self.config.STEPS_PER_EPOCH,
            callbacks=callbacks,
            validation_data=next(val_generator),
            validation_steps=self.config.VALIDATION_STEPS,

        )
        #self.epoch = max(self.epoch, epochs)

    def detect(self, images, verbose=0):
        """Runs the detection pipeline.

        images: List of images, potentially of different sizes.

        Returns a list of dicts, one dict per image. The dict contains:
        rois: [N, (y1, x1, y2, x2)] detection bounding boxes
        class_ids: [N] int class IDs
        scores: [N] float probability scores for the class IDs
        masks: [H, W, N] instance binary masks
        """

        # Validate image sizes
        # All images in a batch MUST be of the same size
        image_shape = np.array([144, 160, 160])

        # Anchors

        # Run object detection
        # rpn_class_logits, rpn_class, rpn_bbox =\
        #     self.keras_model.predict([images], verbose=0)
        heatmap, Y1 = \
            self.keras_model.predict([images], verbose=0)
        #self.keras_model.summary(), Y1

        # Process detections
        results = []
        for i, image in enumerate(images):
            # final_rpn_class_logits, final_rpn_class, final_rpn_bbox, =\
            #     self.unmold_detections(detections[i], mrcnn_mask[i],
            #                            image.shape)
            results.append({
                "heatmap": heatmap[i],
                #"lmk": Y2[i],
                "cls": Y1[i]
                # "rpn_bbox": rpn_bbox[i]
            })
        return results
    def summary(self, images, verbose=0):
        """Runs the detection pipeline.

        images: List of images, potentially of different sizes.

        Returns a list of dicts, one dict per image. The dict contains:
        rois: [N, (y1, x1, y2, x2)] detection bounding boxes
        class_ids: [N] int class IDs
        scores: [N] float probability scores for the class IDs
        masks: [H, W, N] instance binary masks
        """

        # Validate image sizes
        # All images in a batch MUST be of the same size
        image_shape = np.array([64, 64, 64])

        # Anchors
        anchors = self.get_anchors(image_shape)
        # Duplicate across the batch dimension because Keras requires it
        # TODO: can this be optimized to avoid duplicating the anchors?
        anchors = np.broadcast_to(anchors, (1,) + anchors.shape)

        # Run object detection
        rpn_class_logits, rpn_class, rpn_bbox = \
            self.keras_model.predict([images, anchors], verbose=0)
        results = []
        for i, image in enumerate(images):
            # final_rpn_class_logits, final_rpn_class, final_rpn_bbox, =\
            #     self.unmold_detections(detections[i], mrcnn_mask[i],
            #                            image.shape)
            results.append({
                "rpn_logits": rpn_class_logits[i],
                "rpn_class_ids": rpn_class[i],
                "rpn_bbox": rpn_bbox[i]
            })
        feature_map = self.keras_model.get_layer('rpn_class_raw')
        return results, feature_map



    def features_maps(self, images, verbose=0):
        """Runs the detection pipeline.

        images: List of images, potentially of different sizes.

        Returns a list of dicts, one dict per image. The dict contains:
        rois: [N, (y1, x1, y2, x2)] detection bounding boxes
        class_ids: [N] int class IDs
        scores: [N] float probability scores for the class IDs
        masks: [H, W, N] instance binary masks
        """

        # Validate image sizes
        # All images in a batch MUST be of the same size
        image_shape = np.array([144, 192, 192])

        # Anchors
        anchors = self.get_anchors(image_shape)
        # Duplicate across the batch dimension because Keras requires it
        # TODO: can this be optimized to avoid duplicating the anchors?
        anchors = np.broadcast_to(anchors, (1,) + anchors.shape)


        # Run object detection
        feature_maps =\
            self.keras_model.predict([images, anchors], verbose=0)
        #self.keras_model.summary()

        # Process detections
        # results = []
        # for i, image in enumerate(images):
        #     # final_rpn_class_logits, final_rpn_class, final_rpn_bbox, =\
        #     #     self.unmold_detections(detections[i], mrcnn_mask[i],
        #     #                            image.shape)
        #     results.append({
        #         "rpn_logits": rpn_class_logits[i],
        #         "rpn_class_ids": rpn_class[i],
        #         "rpn_bbox": rpn_bbox[i]
        #     })
        return feature_maps
    def unmold_detections(self, detections, mrcnn_mask, original_image_shape):
        """Reformats the detections of one image from the format of the neural
        network output to a format suitable for use in the rest of the
        application.

        detections: [N, (z1, y1, x1, z2, y2, x2, class_id, score)] in normalized coordinates
        mrcnn_mask: [N, depth, height, width, num_classes]
        original_image_shape: [D, H, W, C] Original image shape before resizing
        image_shape: [D, H, W, C] Shape of the image after resizing and padding
        window: [z1, y1, x1, z2, y2, x2] Pixel coordinates of box in the image where the real
                image is excluding the padding.

        Returns:
        boxes: [N, (z1, y1, x1, z2, y2, x2)] Bounding boxes in pixels
        class_ids: [N] Integer class IDs for each bounding box
        scores: [N] Float probability scores of the class_id
        masks: [depth, height, width, num_instances] Instance masks
        """
        # How many detections do we have?
        # Detections array is padded with zeros. Find the first class_id == 0.
        zero_ix = np.where(detections[:, 6] == 0)[0]
        N = zero_ix[0] if zero_ix.shape[0] > 0 else detections.shape[0]

        # Extract boxes, class_ids, scores, and class-specific masks
        boxes = detections[:N, :6]
        class_ids = detections[:N, 6].astype(np.int32)
        scores = detections[:N, 7]
        masks = mrcnn_mask[np.arange(N), :, :, :, class_ids]
        window = np.array([0, 0, 0, 1, 1, 1])
        # Translate normalized coordinates in the resized image to pixel
        # coordinates in the original image before resizing
        # window = utils.norm_boxes(window, image_shape[:2])
        # wy1, wx1, wy2, wx2 = window
        # shift = np.array([wy1, wx1, wy1, wx1])
        # wh = wy2 - wy1  # window height
        # ww = wx2 - wx1  # window width
        # scale = np.array([wh, ww, wh, ww])
        # # Convert boxes to normalized coordinates on the window
        # boxes = np.divide(boxes - shift, scale)
        # Convert boxes to pixel coordinates on the original image
        boxes = utils.denorm_boxes(boxes, np.array([64,64,64]))

        # Filter out detections with zero area. Happens in early training when
        # network weights are still random
        exclude_ix = np.where(
            (boxes[:, 3] - boxes[:, 0]) * (boxes[:, 4] - boxes[:, 1]) * (boxes[:, 5] - boxes[:, 2]) <= 0)[0]
        if exclude_ix.shape[0] > 0:
            boxes = np.delete(boxes, exclude_ix, axis=0)
            class_ids = np.delete(class_ids, exclude_ix, axis=0)
            scores = np.delete(scores, exclude_ix, axis=0)
            masks = np.delete(masks, exclude_ix, axis=0)
            N = class_ids.shape[0]

        # Resize masks to original image size and set boundary threshold.
        # full_masks = []
        # for i in range(N):
        #     # Convert neural network mask to full size mask
        #     full_mask = utils.unmold_mask(masks[i], boxes[i], original_image_shape)
        #     full_masks.append(full_mask)
        # full_masks = np.stack(full_masks, axis=-1)\
        #     if full_masks else np.empty(original_image_shape[:2] + (0,))

        return boxes, class_ids, scores


